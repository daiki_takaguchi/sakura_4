﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;

namespace SAKURA.UNITY_EDITOR{

	public class Csv_functions : MonoBehaviour {




		private static List<string> load_file(string file_name){

			List<string> output = new List<string>();

			string line;

			//print (file_name);

			try 
			{
				if (File.Exists(file_name)) {

					print (file_name);

					StreamReader sr = new System.IO.StreamReader(file_name);

					while((line = sr.ReadLine()) != null){
						//print (line);

						output.Add(line);
					}
				}

			}catch{
			}
				


		

			return output;
		}

		private static StreamWriter write_file(string file_name, bool append){

			StreamWriter output = new StreamWriter (file_name, append);

			return output;

		}

		public static List<string> load_one_row(string one_row){
			List<string> output = new List<string> ();

			string temp = one_row;

			bool is_data_string = false;
			bool is_double_quoted_data_string = false;
			bool is_prev_char_double_quote = false;

			string row_data = "";


			for (int i = 0; i < temp.Length; i++){

				if(is_data_string){

					if(is_double_quoted_data_string){

						if(is_prev_char_double_quote){

							if(temp[i]=='\"'){
								is_prev_char_double_quote =false;
								row_data+="\"";

							} else if(temp[i]==','){

								output.Add (row_data);

								is_data_string=false;

							}else if(temp[i]=='\r' ||temp[i]=='\n'){


								Debug.LogError("csv parse error!");
								
							}else{

								Debug.LogError("csv parse error!");



							}
						}else{

							if(temp[i]=='\"'){
								is_prev_char_double_quote =true;

							}else{
								is_prev_char_double_quote =false;
								row_data+=temp[i];
							}
						}

					}else{

						if(temp[i]==','){

							output.Add (row_data);
								
							is_data_string=false;

						}else if(temp[i]=='\r' ||temp[i]=='\n'){
							output.Add (row_data);
							break;


						}else{
						
							row_data+=temp[i];
						}


					}
				
				
				}else{



					is_prev_char_double_quote =false;
					
					if(temp[i]=='\"'){
						row_data="";

						is_data_string=true;
						
						is_double_quoted_data_string=true;

					}else if (temp[i]==',') {


						row_data="";

						output.Add (row_data);

						is_data_string=false;
						
					}else{

						row_data="";

						is_data_string=true;
						
						is_double_quoted_data_string=false;
						

					}

				}
			
			}

			output.Add (row_data);

		

			if(!is_prev_char_double_quote){

				throw new Csv_exception();

			}


			return output;


		}

		public static List<string> split_by_first_comma(string input_string){
			List<string> output = new List<string> ();



			return output;

		}

		public static string save_one_row(List<string> one_row){
			string output = "\"";

			bool is_first_row = true;

			foreach(string one_data in one_row){

				if(is_first_row){
					is_first_row=false;
					
				}else{
					output+="\",\"";
				}

				output+=one_data.Replace("\"","\"\"");


			}

			output += "\"\r\n";

			return output;
		}


		public static List<List<string>>load(string file_name){

			List<List<string>> output = new List<List<string>> ();

			List<string> loaded_file =load_file (file_name);

			string prev_text = "";

			for (int i = 1; i < loaded_file.Count; i++){

				try{

					List<string> temp_row=load_one_row(prev_text+loaded_file[i]);

					output.Add(temp_row);
					/*
					string ss = "";
					foreach (string s in temp_row) {
						ss+=s;
					}

					print (ss);*/


					prev_text="";

				}catch (Csv_exception ex){

					prev_text+=loaded_file[i];

					//print ("error");
				}

			}
			

			



			return output;

		}



		public static bool add(string file_name, List<string> data){

			bool output = true;


			return output;
		}



		public static bool over_write(string file_name, List<List<string>> data){

			bool output = true;
			
			
			return output;

		}


	}


}
