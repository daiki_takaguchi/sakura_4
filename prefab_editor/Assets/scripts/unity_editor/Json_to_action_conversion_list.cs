﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using A=SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.UNITY_EDITOR{

	[ExecuteInEditMode]
	public class Json_to_action_conversion_list : MonoBehaviour {

		public bool export_structure_json;

		public string json_file_name;

		public List<Json_to_action_conversion> csv_to_action_conversion_list ;


		
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){

				if(export_structure_json){

					save_structure(json_file_name);

					export_structure_json=false;
				}

			}
		
		}

		public void convert(){

		}

		public void convert_exception(string action_name,string data){



		}

		public void save_data(string file_name,List<A.Action> input_action_list){


			FileStream fs = new FileStream(file_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);
			
			JSONObject j = new JSONObject(JSONObject.Type.OBJECT);


			foreach (A.Action action in input_action_list) {


				bool is_finished=false;
				string action_name="";

				foreach (Json_to_action_conversion c_to_a in csv_to_action_conversion_list) {

					if(action.GetType().Name==c_to_a.action_name){

						action_name=c_to_a.action_name;

						int count=0;

						JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);

						foreach (string variable_name in c_to_a.variable_name){

							Type variable_type=variable_type_to_type( c_to_a.variable_type[count]);


							string value= Sakura.get_variable_value(action,action.GetType(),variable_name,variable_type);

							

							//JSONObject data = new JSONObject(JSONObject.Type.OBJECT);

							arr.AddField(variable_name,value);





							count++;


						}

						j.AddField(action_name,arr);

						is_finished=true;

						break;
					}

				}


				if(is_finished){

				}else{
					Debug.LogError("conversion error!");
				}


			}


			//print (j.ToString());

			sw.WriteLine(j.ToString());    

			sw.Flush();
			sw.Close(); 
			fs.Close ();



		}

		public List<A.Action> load_data(string file_name,Transform action_list_transform){

			List<A.Action> output = new List<A.Action> ();



			return output;

		}

		public void save_structure(string input_data_name){
			
			
	
	
			FileStream fs = new FileStream(input_data_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);
			
			int count = 0;
			
			JSONObject j = new JSONObject(JSONObject.Type.OBJECT);
			
			
			foreach(Json_to_action_conversion temp in csv_to_action_conversion_list){
				
				if(temp!=null){

					int action_count=0;

					JSONObject action_data = new JSONObject(JSONObject.Type.ARRAY);

					foreach(string variable_name in temp.variable_name){

						JSONObject arr=variable_type_to_json(variable_name,temp.variable_type[action_count]);
						
						action_data.AddField(action_count.ToString(),arr);

						action_count++;

					

					}


					j.AddField(temp.action_name,action_data);

					
					
				}


				
				count++;
				
			}
			sw.WriteLine ("var data_structure =");
			sw.WriteLine(j.ToString());     
			
			
			sw.Flush();
			sw.Close(); 
			fs.Close ();
			
		}

		public JSONObject variable_type_to_json(string variable_name, SAKURA.Consts.Unity_editor.Csv_to_action_conversion.Variable_type input_type){

			Type temp = variable_type_to_type (input_type);

			JSONObject output = new JSONObject(JSONObject.Type.OBJECT);

			JSONObject data_name = new JSONObject(JSONObject.Type.ARRAY);

			data_name.AddField ("text",variable_name);

			output.AddField("data_name",data_name);

			if(temp.IsEnum){


				JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);

				int count=0;

				foreach (string arr_name in Enum.GetNames(temp)) {

					JSONObject arr_option = new JSONObject(JSONObject.Type.OBJECT);

					arr_option.AddField("value",arr_name);

					arr_option.AddField("text",arr_name);


					arr.AddField(count.ToString(),arr_option);

					count++;

				}

				output.AddField("data_type","enum" );


				output.AddField("data",arr );



			}else if(temp ==typeof(string)){



				output.AddField("data_type","string" );

			}else if(temp ==typeof(float)){


				output.AddField("data_type","string" );
			}

			return output;

		}


		public Type variable_type_to_type(SAKURA.Consts.Unity_editor.Csv_to_action_conversion.Variable_type input_type){

			Type output = null;

			if (input_type == Consts.Unity_editor.Csv_to_action_conversion.Variable_type.string_type) {
				output= typeof(string);
			}else if(input_type==Consts.Unity_editor.Csv_to_action_conversion.Variable_type.float_type){
				output= typeof(float);
			}else if(input_type==Consts.Unity_editor.Csv_to_action_conversion.Variable_type.person_name_type){
				output= typeof(Consts.Data.Scene_settings.Action.Person_name);
			}

			return output;
		}

	
	}
}
