﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using UnityEditor;


namespace SAKURA.UNITY_EDITOR{
[ExecuteInEditMode]
[InitializeOnLoad]
public class Object_prefab_list : MonoBehaviour {


		public bool save_data_now;
		public bool load_data_now;
		public bool load_data_confirm;


		[HideInInspector]public Transform objects_parent_transform;

		public List<string> prefab_folder_list;

	//	public TextAsset loaded_object_data;
		public string object_data_folder;
		public string object_data_name="object_data.json";


		public List<Object_prefab> created_object_list;
		
		public List<Object_prefab> prefab_list;
		//public List<string> object_link_list;


		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type guid= Consts.Unity_editor.Object_prefab.Data_type.prefab_GUID;

		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type object_name= Consts.Unity_editor.Object_prefab.Data_type.object_name;

		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type prefab_type= Consts.Unity_editor.Object_prefab.Data_type.prefab_type;
		
		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type object_position= Consts.Unity_editor.Object_prefab.Data_type.local_position;

		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type object_rotation= Consts.Unity_editor.Object_prefab.Data_type.local_rotation;

		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type object_scale= Consts.Unity_editor.Object_prefab.Data_type.local_scale;

		private SAKURA.Consts.Unity_editor.Object_prefab.Data_type object_grid=Consts.Unity_editor.Object_prefab.Data_type.grid_length;


		// Use this for initialization
		void Start () {

		}

		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){

				created_object_list= new List<Object_prefab>();

				objects_parent_transform=transform;

				foreach(Transform temp in objects_parent_transform){
					Object_prefab temp_2=temp.GetComponent<Object_prefab>();
					created_object_list.Add (temp_2);
				}

				if(save_data_now){

					save_data(object_data_folder+object_data_name);
					save_data_now=false;

				}else if(!load_data_now){
					save_data(object_data_folder+"autosave.json");
				}

				if(load_data_now&&load_data_confirm){
					
					load_data();
					
					load_data_now=false;
					load_data_confirm=false;
				}

			}
		}

		public void load_prefab(){
			
			
			prefab_list = new List<Object_prefab> ();
			
			foreach (string dir_path in prefab_folder_list){
				
				//DirectoryInfo di = new DirectoryInfo(folder);
				
				string total_dir_path=Application.dataPath+dir_path;
				
				string[] fp = Directory.GetFiles(total_dir_path);
				
				
				
				foreach (string total_file_path in fp) {
					
					FileInfo fi = new FileInfo(total_file_path);
					
					string unity_file_path= "Assets"+dir_path+fi.Name;
					
					string file_guid= AssetDatabase.AssetPathToGUID(unity_file_path);
					
					//print(unity_file_path);
					
					GameObject g =(GameObject) Resources.LoadAssetAtPath<GameObject>(unity_file_path);
					
					if(g!=null){
						
						Object_prefab p=g.GetComponent<Object_prefab>();
						
						if(p==null){
							
							p=g.AddComponent<Object_prefab>();
							
							
						}
						
						prefab_list.Add (p);
						p.prefab_guid=file_guid;
						p.prefab=g;
						p.prefab_name=g.name;
						
						
					}
				}
			}

		}

		public void load_data(){

			if(objects_parent_transform==null){
				objects_parent_transform=this.transform;
			}

			load_prefab ();
		



			foreach(Object_prefab temp in created_object_list){

				if(temp!=null){
					DestroyImmediate(temp.gameObject);
				}
			}

			created_object_list = new List<Object_prefab> ();

			string json_file_path = Application.dataPath + object_data_folder + object_data_name;

			FileStream fs = new FileStream(json_file_path,FileMode.Open, FileAccess.Read);

			StreamReader sw = new StreamReader (fs);

			JSONObject j = new JSONObject(sw.ReadToEnd());

			foreach (JSONObject one_object in j.list ){
					
				GameObject created_gameobject=null;
				GameObject original_gameobject=null;

					for(int i = 0; i < one_object.list.Count; i++){
						
						string key= one_object.keys[i];
						
						
						if(key==guid.ToString()){
							string value= one_object.list[i].str;

							original_gameobject=Resources.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(value));
							created_gameobject= (GameObject)  PrefabUtility.InstantiatePrefab(original_gameobject);
							created_gameobject.transform.parent=objects_parent_transform;
						}

					}

					if(created_gameobject!=null){

					Object_prefab created_prefab= created_gameobject.GetComponent<Object_prefab>();

						if(created_prefab!=null){

							created_prefab.prefab=original_gameobject;
							
							created_object_list.Add (created_prefab);


							for(int i = 0; i < one_object.list.Count; i++){
								
								string key= one_object.keys[i];
							
								if(key==object_name.ToString()){
									string value= one_object.list[i].str;
									
									created_prefab.name=value;
								}else if(key==prefab_type.ToString()){

									string value= one_object.list[i].str;

									created_prefab.prefab_type= (SAKURA.Consts.Unity_editor.Object_prefab.Prefab_type) System.Enum.Parse( typeof( SAKURA.Consts.Unity_editor.Object_prefab.Prefab_type ), value );
								
								}else if(key==object_position.ToString()){

									created_prefab.local_position=json_to_vector3(one_object.list[i]);

								}else if(key==object_rotation.ToString()){
									
									created_prefab.local_rotation=json_to_vector3(one_object.list[i]);
								
								}else if(key==object_scale.ToString()){

									created_prefab.local_scale=json_to_vector3(one_object.list[i]);

								}else if(key==object_grid.ToString()){

									created_prefab.grid_length=json_to_vector3(one_object.list[i]);
								}

								
								
							}

							created_prefab.refresh();
						}



					}


			}

		}
	

		public void save_data(string input_data_name){

			load_prefab ();

			string file_name=Application.dataPath+input_data_name;
			string file_name_back=Application.dataPath+input_data_name+".back";

			string[] names =Enum.GetNames (typeof(SAKURA.Consts.Unity_editor.Object_prefab.Data_type));

			File.Delete(file_name_back);



			if(File.Exists(file_name)){
				File.Move(file_name,file_name_back);
			}

			FileStream fs = new FileStream(file_name,FileMode.Create, FileAccess.Write);

			StreamWriter sw = new StreamWriter (fs);

			int count = 0;

			JSONObject j = new JSONObject(JSONObject.Type.OBJECT);


			foreach(Object_prefab temp in created_object_list){
			
				if(temp!=null){
					JSONObject arr = new JSONObject(JSONObject.Type.ARRAY);

					arr.AddField(guid.ToString(),temp.prefab_guid);

					arr.AddField(object_name.ToString(),temp.name);

					arr.AddField (prefab_type.ToString(),temp.prefab_type.ToString());

					arr.AddField(object_position.ToString(),vector3_to_json(temp.local_position));

					arr.AddField(object_rotation.ToString(),vector3_to_json(temp.local_rotation));

					arr.AddField(object_scale.ToString(),vector3_to_json(temp.local_scale));

					arr.AddField(object_grid.ToString(),vector3_to_json(temp.grid_length));


					j.AddField(count.ToString(),arr);



					count++;
				}

			}

			sw.WriteLine(j.ToString());     
			

			sw.Flush();
			sw.Close(); 
			fs.Close ();

		}

		public JSONObject vector3_to_json(Vector3 input_vector){

			JSONObject output = new JSONObject(JSONObject.Type.ARRAY);

			output.AddField ("x", input_vector.x);
			output.AddField ("y", input_vector.y);
			output.AddField ("z", input_vector.z);

			return output;
			
		}

		public Vector3 json_to_vector3(JSONObject input_json){
			
			//Vector3 output = new Vector3(0,0,0);

			float x=0;
			float y=0;
			float z = 0;
			
			for(int i = 0; i < input_json.list.Count; i++){
				
				string key= input_json.keys[i];
				
				if(key=="x"){
					x=input_json.list[i].n;
				}else if(key=="y"){
					y=input_json.list[i].n;
				}else if(key=="z"){
					z=input_json.list[i].n;
				}

			}

		
		
			return new Vector3 (x,y,z);
		}
	}
			


}
