﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA{
	public class Resources : MonoBehaviour {

		public static Resources resources_static;

		public Data data;
		public RESOURCES.People_data people_data;
		// Use this for initialization
		void Start () {
			if(resources_static==null){
				resources_static=this;
			}
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public Texture get_person_image(SAKURA.Consts.Data.Scene_settings.Action.Person_name input_name,
		                                SAKURA.Consts.Data.Scene_settings.Action.Person_image_expression input_expression){

			return people_data.get_person_image (input_name, input_expression);
		}

		public string get_person_name(SAKURA.Consts.Data.Scene_settings.Action.Person_name input_name){

			return people_data.get_person_name (input_name);
		}
	}
}
