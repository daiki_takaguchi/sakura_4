﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.UI.OBJECTS;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_image : Variable {


		//public float initial_value;
		//public float current_value;

		//public Texture texture;

		[SerializeField] public SAKURA.Consts.Data.Game_event.Variable.Variable_image_state current_image_state;
		[SerializeField] private SAKURA.Consts.Data.Scene_settings.Action.Fade_mode fade_mode;
		[SerializeField] private float fade_time;
		[SerializeField] private float fade_start_time;
		[SerializeField] private float alpha;

		public Image image;

		public float x;
		public float y;

		// Use this for initialization
		void Start () {
		
		}

		void Update () {

			if(!Application.isPlaying){
				//if(waiting_game_event_list==null){
				refresh_children();
				//}
			}else{
				if(current_image_state==Consts.Data.Game_event.Variable.Variable_image_state.fading_in){
					float time_from_start=Time.time-fade_start_time;

					//print (time_from_start);
					if(time_from_start>fade_time){
						change_state(Consts.Data.Game_event.Variable.Variable_image_state.shown);
					}else{
						if(fade_mode==Consts.Data.Scene_settings.Action.Fade_mode.alpha){
							change_alpha(time_from_start/fade_time);
						}
					}

				}else if(current_image_state==Consts.Data.Game_event.Variable.Variable_image_state.fading_out){

					float time_from_start=Time.time-fade_start_time;
					
					if(time_from_start>fade_time){
						change_state(Consts.Data.Game_event.Variable.Variable_image_state.hidden);
					}else{
						if(fade_mode==Consts.Data.Scene_settings.Action.Fade_mode.alpha){
							change_alpha(1-(time_from_start/fade_time));
						}
					}
				}
			}
		}

		public void change_state(SAKURA.Consts.Data.Game_event.Variable.Variable_image_state input_state,
		                         SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode,
		                         float input_fade_time){
				
			change_fade_mode (input_fade_mode);
			change_fade_time (input_fade_time);
			change_state (input_state);

		}

		public void change_state(SAKURA.Consts.Data.Game_event.Variable.Variable_image_state input_state){

			current_image_state = input_state;

			if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.start_fade_in&&fade_time<=0){
				current_image_state=Consts.Data.Game_event.Variable.Variable_image_state.shown;
			}else if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out&&fade_time<=0){
				current_image_state=Consts.Data.Game_event.Variable.Variable_image_state.hidden;
			}

			if(image!=null){



				if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.shown){
					change_alpha(1.0f);
					image.gameObject.SetActive(true);
				}else if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.hidden){
					image.gameObject.SetActive(false);
				}else if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.start_fade_in){
					change_alpha(0f);
					image.gameObject.SetActive(true);
					fade_start_time=Time.time;
					current_image_state=Consts.Data.Game_event.Variable.Variable_image_state.fading_in;
				
				}else if(input_state==Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out){
					change_alpha(1.0f);
					fade_start_time=Time.time;
					current_image_state=Consts.Data.Game_event.Variable.Variable_image_state.fading_out;
				}
			}

			waiting_game_event_list.initalize_action_pointer ();
			waiting_game_event_list.run_events ();

		}


		public bool is_shown(){
			bool output = false;

			if(current_image_state==Consts.Data.Game_event.Variable.Variable_image_state.shown){
				output=true;
			}

			return output;
		}

		public void change_texture(Texture input_texture){
			
			
			if(image!=null){
				image.uitexture.mainTexture=input_texture;
			}
		}

		public void change_fade_mode( SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode){
			fade_mode = input_fade_mode;
		}

		public void change_fade_time(float input_fade_time){
			fade_time = input_fade_time;
		}

		public void change_alpha(float input_alpha){

			alpha = input_alpha;

			if(image!=null){
				image.uiwidget.alpha=alpha;
			}
		}
		
	
	}
}
