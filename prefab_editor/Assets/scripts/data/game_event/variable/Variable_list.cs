﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	[ExecuteInEditMode]
	public class Variable_list : MonoBehaviour {

		public Consts.Data.Game_event.Variable.Variable_scope_type variable_scope_type;
		public List<Variable> variable_list;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {


			if(!Application.isPlaying){

				refresh_children();

				Gameplay temp=GameObject.FindObjectOfType<Gameplay>();

				if(temp!=null){
	
					if(!temp.all_variable_lists.Contains(this)){

						temp.all_variable_lists.Add(this);
					}
				}

			}
		}

		public void refresh_children(){
			variable_list = Sakura.get_monos_from_children<Variable> (transform);
			
			foreach (Variable variable in variable_list){
				variable.name=variable.variable_name;
				variable.variable_scope_type=variable_scope_type;
			}

		}

		public bool remove_and_destroy_Variable(string variable_name){

			bool output = false;

			Variable temp = remove_variable (variable_name);

			if(temp!=null){

				Destroy(temp);
				output=true;

			}else{
				output=false;
			}

			return output;
		
		}

		public Variable remove_variable(string variable_name){

			Variable output = remove_variable (variable_name);

			if(output!=null){
				variable_list.Remove(output);

			}

			return output;
		}

		public T create_variable<T>(string variable_name) where T: Variable{


			GameObject temp = new GameObject ();
			 
			temp.transform.parent = transform;
			temp.name = variable_name;
			T output = (T) temp.AddComponent<T>();
			output.variable_name = variable_name;
			output.initialize ();

			variable_list.Add (output);

			return output;
		}

		public Variable get_variable(string variable_name){

			Variable output=null;
			foreach(Variable variable in variable_list){

				if(variable.name==variable_name){
					if(output==null){
						output=variable;
					}else{
						Debug.LogError("duplicate varible name!");
					}
				}
			}

			return output;
		}

		public void OnDestroy(){


			Gameplay temp=GameObject.FindObjectOfType<Gameplay>();
			
			if(temp!=null){
				
				if(!temp.all_variable_lists.Contains(this)){
					
					temp.all_variable_lists.Remove(this);
				}
			}
		}


	}
}
