﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.CONDITION;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	[ExecuteInEditMode]
	public class Variable : MonoBehaviour {

		public Consts.Data.Game_event.Variable.Variable_scope_type variable_scope_type;
		public Game_event_list waiting_game_event_list=null;
		public string variable_name;
		//public List<Game_event> game_events_to_set_waiting_list = new List<Game_event> ();
		//public List<Action> all_referencing_actions;
		//public List<Action> current_referencing_actions;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if(!Application.isPlaying){
				//if(waiting_game_event_list==null){
				refresh_children();
				//}
			}
		}


		public void add_game_event(Game_event input_game_event){
			/*
			if(waiting_game_event_list!=null){
				waiting_game_event_list.add_game_event(input_game_event);
			}*/

			waiting_game_event_list.add_game_event (input_game_event);
		}

		public void refresh_children(){


			foreach(Transform child in transform){
				DestroyImmediate(child.gameObject);
			}

			GameObject temp= new GameObject();
			temp.transform.parent=transform;
			waiting_game_event_list=temp.AddComponent<Game_event_list>();
			temp.name= "event_list: "+this.GetType().Name;

		
		}

		/*public virtual void change_value(Consts.Ui.Objects.Touch_panel_state input_state){

		}*/

		public virtual void initialize(){

		}
	}
}
