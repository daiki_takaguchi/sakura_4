﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.CONDITION{
	public class Condition_image : Condition {

		// Use this for initialization

		//public Variable_item referencing_variable_item;

		public Variable_image variable_image;
		public SAKURA.Consts.Data.Game_event.Variable.Variable_image_state variable_image_state;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override Variable add_event_to_variable(Game_event input_game_event){

			if(variable_image!=null){
				variable_image.add_game_event(input_game_event);
			}

			return variable_image;
		}

		public override bool check_condition(){
			
			bool output = false;

			if(variable_image!=null){

				output=(variable_image.current_image_state==variable_image_state);
			

			}


			
			return output;
		}
	}
}
