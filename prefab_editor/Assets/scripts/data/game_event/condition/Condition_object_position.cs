﻿using UnityEngine;
using System.Collections;
using SAKURA;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.CONDITION{
	public class Condition_object_position :Condition {
		public Variable_object_position variable_object_position;

		public SAKURA.Consts.Data.Game_event.Variable.Variable_object_position_state condition_state;
	
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override Variable add_event_to_variable(Game_event input_game_event){

			variable_object_position.add_game_event (input_game_event);
			
			return variable_object_position;
		}
		
		public override bool check_condition(){
			bool output = false;

			if(variable_object_position.state==condition_state){
				output=true;
			}

			return output;
		}
	}
}
