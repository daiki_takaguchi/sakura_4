﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.ACTION{


	public class Action : MonoBehaviour {

		public string tag;


		public int action_list_order;
		[HideInInspector] public Action_list parent_action_list;
		[HideInInspector] public Action parent_action;
		[HideInInspector] public Action child_action;
		[HideInInspector] public Game_event_list created_game_event_list=null;


	

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

		
		}

		
		public virtual void parse(string input_string){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
			//Ui.jump(jump_layer_type);
			//}
			
			//action_calling = input_action;
			
			//return true;
		}


		public virtual void run(Action input_action){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
			//Ui.jump(jump_layer_type);
			//}

			//action_calling = input_action;

			//return true;
		}

		public virtual void run(Action_list input_action_list){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
				//Ui.jump(jump_layer_type);
			//}
			/*
			if(action_calling!=null){
				action_calling.run_finished(this);
				action_calling=null;
			}*/

			//return true;
		}

		public virtual bool save(string file_name){
			bool output = true;

			return output;
		}

		public virtual bool load(string file_name){
			bool output = true;
			
			return output;
		}

		public virtual void variable_changed(Variable input_variable,Game_event input_game_event){
				
		}

		public virtual void child_action_finished(Action input_child_action){

		}

	}
}
