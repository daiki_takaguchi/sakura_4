﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	[ExecuteInEditMode]
	public class Unlock_memo : MonoBehaviour {

		// Use this for initialization
		/*
		public virtual  SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.unlock_memo;}
		}*/


		public SAKURA.DATA.MEMO_DATA.Memo_data memo_data;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if (!Application.isPlaying) {
				if(memo_data!=null){
					memo_data.unlock_memo=this;
				}
			}
		}

		public virtual bool run(){
			//if(action_type==Consts.Data.Scene_settings.Action.Action_type.Jump){
				//Ui.jump(jump_layer_type);
			//}

			return true;
		}

		public virtual void run_finished(){

		}

		public virtual void transfer_current_touch_panel_state(Consts.Ui.Objects.Touch_panel_state input_touch_panel_state){

		}

	}
}
