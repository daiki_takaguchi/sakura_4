﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.UI.LAYER;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	public class Select : Action {

		public SAKURA.Consts.Data.Scene_settings.Action.Person_name person_name;

		public string select_text_0;
		public string select_text_1;
		public string select_text_2;
		public string jump_tag_0;
		public string jump_tag_1;
		public string jump_tag_2;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {



		
		}


		IEnumerator run_again(float wait_time) {
			yield return new WaitForSeconds(wait_time);
			run_internal (true);
		}


		public override void run(Action input_action){

			parent_action = input_action;

			run_internal (false);
		}

		public override void run(Action_list input_action_list){

			parent_action_list = input_action_list;

			run_internal (false);

		}

		public void run_internal(bool is_internal){

			SAKURA.Ui.ui_static.show_if_hidden (Consts.Ui.Layer_type.adv_text);

			
			Adv_text_layer adv_text = (Adv_text_layer) Ui.ui_static.layer_list.get_layer (Consts.Ui.Layer_type.adv_text);

			adv_text.set_name (person_name);

			List<string> input_text_list = new List<string> ();
			List<string> input_tag_list= new List<string> ();

			input_text_list.Add (select_text_0);
			input_text_list.Add (select_text_1);
			input_text_list.Add (select_text_2);

			input_tag_list.Add (jump_tag_0);
			input_tag_list.Add (jump_tag_1);
			input_tag_list.Add (jump_tag_2);

			adv_text.show_select (input_text_list,input_tag_list);

		}


	

	}



}
