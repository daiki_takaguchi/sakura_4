﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.SCENE_SETTINGS;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Jump : Action {


		public SAKURA.Consts.Data.Scene_settings.Action.Jump_type jump_type;
		public string destination;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override void run(Action_list input_action_list){

			if(jump_type==Consts.Data.Scene_settings.Action.Jump_type.tag){
				Jump_tag_list temp=(Jump_tag_list) FindObjectOfType<Jump_tag_list>();

				temp.jump(destination);
			}

			//input_action_list.run_next ();

		}
	}
}
