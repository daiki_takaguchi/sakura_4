using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA;
using SAKURA.UI.LAYER;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Action_person_image : Action {

		/*
		public override SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.change_person_image;}
		}*/

		public SAKURA.Consts.Data.Scene_settings.Action.Person_name person_name;


		public SAKURA.Consts.Data.Scene_settings.Action.Person_image_expression expression;


		public SAKURA.Consts.Data.Scene_settings.Action.Person_image_position position;


		public SAKURA.Consts.Data.Scene_settings.Action.Image_operation operation;


		public SAKURA.Consts.Data.Scene_settings.Action.Fade_mode fade_mode=Consts.Data.Scene_settings.Action.Fade_mode.alpha;


		public float fade_time;
		public float alpha=1.0f;

		public string text;



		//private Change_adv_text change_adv_text;


	 	public SAKURA.Consts.Data.Scene_settings.Action.Person_image_operation_state state=Consts.Data.Scene_settings.Action.Person_image_operation_state.waiting_for_run;

		[HideInInspector] public List<Variable_image> fading_images = new List<Variable_image> ();
		//[HideInInspector] public Game_event notify_me=null;

	
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

		}


		
		public override void run(Action_list input_action_list){
			parent_action_list = input_action_list;
			run_internal (false);
		}



		private void run_internal(bool is_internal){

			bool is_finished = false;

			print (state);
			print (operation);

			Adv_layer adv_layer= (Adv_layer) Ui.ui_static.layer_list.get_layer(SAKURA.Consts.Ui.Layer_type.adv);

			Adv_text_layer adv_text_layer= (Adv_text_layer) Ui.ui_static.layer_list.get_layer(SAKURA.Consts.Ui.Layer_type.adv_text);
			
			Variable_image variable_image= (Variable_image) adv_layer.get_variable (position);


			if (state == Consts.Data.Scene_settings.Action.Person_image_operation_state.waiting_for_run) {


				if(operation==Consts.Data.Scene_settings.Action.Image_operation.show
				   ||operation==Consts.Data.Scene_settings.Action.Image_operation.fade_in){


					if(hide_if_overlap(position)){

						state=Consts.Data.Scene_settings.Action.Person_image_operation_state.hiding_previous_image;

					}else{

						state=Consts.Data.Scene_settings.Action.Person_image_operation_state.ready_to_show_image;
					}
				

				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.hide){



					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.hidden);

					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.image_operation_finished;

				
				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.fade_out){

				

					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.running_image_operation;
					
					notify_me_in_condition(variable_image,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
					
					fading_images.Add (variable_image);
					
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);




				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.change){

					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.image_operation_finished;


					variable_image.change_texture(Resources.resources_static.get_person_image(person_name,expression));


				}
			}



			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.hiding_previous_image){



			}

			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.ready_to_show_image){


				
				if(operation==Consts.Data.Scene_settings.Action.Image_operation.show){
					
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.image_operation_finished;
					
					variable_image.change_alpha(alpha);
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.shown);
					
		
				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.fade_in){
					
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.running_image_operation;
					
					notify_me_in_condition(variable_image,Consts.Data.Game_event.Variable.Variable_image_state.shown);
					
					fading_images.Add (variable_image);
					
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_in,fade_mode,fade_time);
					
					
					
				}
				
				
			}

			if (state==Consts.Data.Scene_settings.Action.Person_image_operation_state.running_image_operation){
			
			
			
			}
			
			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.image_operation_finished){
				
				if(text==""){

					is_finished=true;

					adv_text_layer.clear_text();

					Ui.ui_static.hide (Consts.Ui.Layer_type.adv_text);

				}else{
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.creating_text;
				}

			
			}



			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.creating_text){

				GameObject temp_gameobject= new GameObject();
				temp_gameobject.transform.parent=transform;

				Action_adv_text temp=temp_gameobject.AddComponent<Action_adv_text>();

				temp.initalize(person_name,text);

				child_action= temp;

				child_action.run (this);
	
				state=Consts.Data.Scene_settings.Action.Person_image_operation_state.showing_text;

			}

			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.showing_text){

			}

			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.text_operation_finished){
				is_finished=true;
			}

			if(is_finished){
				state=Consts.Data.Scene_settings.Action.Person_image_operation_state.none;

				Destroy(created_game_event_list);

				if(parent_action_list!=null){
					parent_action_list.run_next ();
				}
			}

			//return is_finished;
		}



		public bool hide_if_overlap(
				SAKURA.Consts.Data.Scene_settings.Action.Person_image_position input_position
			){

			bool output = false;

			Adv_layer adv_layer =(Adv_layer)  SAKURA.Ui.ui_static.layer_list.get_layer (Consts.Ui.Layer_type.adv);

			Variable_image variable_image_C =(Variable_image) adv_layer.get_variable(Consts.Data.Scene_settings.Action.Person_image_position.C);
			Variable_image variable_image_L =(Variable_image) adv_layer.get_variable(Consts.Data.Scene_settings.Action.Person_image_position.L);
			Variable_image variable_image_L2 =(Variable_image) adv_layer.get_variable(Consts.Data.Scene_settings.Action.Person_image_position.L2);
			Variable_image variable_image_R =(Variable_image)  adv_layer.get_variable(Consts.Data.Scene_settings.Action.Person_image_position.R);
			Variable_image variable_image_R2 =(Variable_image) adv_layer.get_variable(Consts.Data.Scene_settings.Action.Person_image_position.R2);


			bool C = (variable_image_C.current_image_state == SAKURA.Consts.Data.Game_event.Variable.Variable_image_state.shown);
			bool L = (variable_image_L.current_image_state == SAKURA.Consts.Data.Game_event.Variable.Variable_image_state.shown);
			bool L2 = (variable_image_L2.current_image_state == SAKURA.Consts.Data.Game_event.Variable.Variable_image_state.shown);
			bool R = (variable_image_R.current_image_state == SAKURA.Consts.Data.Game_event.Variable.Variable_image_state.shown);
			bool R2 = (variable_image_R2.current_image_state == SAKURA.Consts.Data.Game_event.Variable.Variable_image_state.shown);

			/*
			print ("c "+C);
			print ("l "+L);
			print ("l2 "+L2);
			print ("r "+R);
			print ("r2 "+R2);
			*/

			if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.C ||
								input_position == Consts.Data.Scene_settings.Action.Person_image_position.L ||
								input_position == Consts.Data.Scene_settings.Action.Person_image_position.R) {

				if(L2||
				  R2){

					output=true;

					if(fading_images.Count==0){

						SAKURA.Ui.ui_static.hide(Consts.Ui.Layer_type.adv_text);

						notify_me_in_condition(variable_image_L2,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
						notify_me_in_condition(variable_image_R2,Consts.Data.Game_event.Variable.Variable_image_state.hidden);


						fading_images.Add (variable_image_L2);
						fading_images.Add (variable_image_R2);

						variable_image_L2.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);
						variable_image_R2.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);

					}else{
						Debug.LogError("error!");
					}
				
				}else{

				}


			}else{

				//print ("okkk");

				if(C||
					R||
				  	L){



					output=true;

					if(fading_images.Count==0){

						SAKURA.Ui.ui_static.hide(Consts.Ui.Layer_type.adv_text);

						notify_me_in_condition(variable_image_C,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
						notify_me_in_condition(variable_image_L,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
						notify_me_in_condition(variable_image_R,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
						
						fading_images.Add (variable_image_C);
						fading_images.Add (variable_image_L);
						fading_images.Add (variable_image_R);

						variable_image_C.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);
						variable_image_L.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);
						variable_image_R.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);
						
					}else{
						Debug.LogError("error!");
					}
					
				}else{

				}
			}

			return output;

		}

		public override void variable_changed(Variable input_variable,Game_event input_game_event){


			if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.hiding_previous_image){

				Variable_image temp= (Variable_image) input_variable;
				
				fading_images.Remove (temp);
				created_game_event_list.remove_game_event (input_game_event);

				if(fading_images.Count==0){
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.ready_to_show_image;
					run_internal(true);
				}
			}else if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.running_image_operation){

				state=Consts.Data.Scene_settings.Action.Person_image_operation_state.image_operation_finished;

				Variable_image temp= (Variable_image) input_variable;

				fading_images.Remove (temp);
				created_game_event_list.remove_game_event (input_game_event);

				run_internal(true);

			}
		}

		public override void child_action_finished(Action input_child_action){

			print (state);

			if(input_child_action==child_action){
				if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.showing_text){
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.text_operation_finished;
					run_internal (true);
				}
			}
		}

		private void notify_me_in_condition(Variable_image input_variable_image,Consts.Data.Game_event.Variable.Variable_image_state input_state){

			if(created_game_event_list==null){
				created_game_event_list=gameObject.AddComponent<Game_event_list>();
			}



			Game_event game_event = created_game_event_list.create_game_event ();

			
			Condition_image condition =(Condition_image) game_event.add_condition<Condition_image>();
			
			condition.variable_image_state = input_state;
			
			condition.variable_image=input_variable_image;

			
			Notify_variable_changed notify =(Notify_variable_changed) game_event.add_action<Notify_variable_changed>();
			
			notify.initialize(this,input_variable_image);

			
			game_event.add_event_to_variables();
		}

	}
}
