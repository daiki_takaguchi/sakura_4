﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA;
using SAKURA.UI.LAYER;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Action_image: Action {

	
		public SAKURA.Consts.Ui.Adv_layer_image_type image_type;
		

		public SAKURA.Consts.Data.Scene_settings.Action.Image_operation operation;

		
		public SAKURA.Consts.Data.Scene_settings.Action.Fade_mode fade_mode=Consts.Data.Scene_settings.Action.Fade_mode.alpha;

		
		public float fade_time;
		public float alpha=1.0f;
		public float x=0.0f;
		public float y=0.0f;

		[HideInInspector] public List<Variable_image> fading_images = new List<Variable_image> ();

		[HideInInspector] public SAKURA.Consts.Data.Scene_settings.Action.Image_operation_state state =SAKURA.Consts.Data.Scene_settings.Action.Image_operation_state.waiting_for_run;



		public override void run(Action_list input_action_list){
			parent_action_list = input_action_list;
			run_internal (false);
		}
		
		
		
		private void run_internal(bool is_internal){
			
			bool is_finished = false;
			

			
			Adv_layer adv_layer= (Adv_layer) Ui.ui_static.layer_list.get_layer(SAKURA.Consts.Ui.Layer_type.adv);

			Variable_image variable_image= (Variable_image) adv_layer.get_variable (image_type);
			
			
			if (state == Consts.Data.Scene_settings.Action.Image_operation_state.waiting_for_run) {
				
				
				if(operation==Consts.Data.Scene_settings.Action.Image_operation.show){

					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.hidden);

				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.fade_in){


					state=Consts.Data.Scene_settings.Action.Image_operation_state.running_image_operation;
					

					notify_me_in_condition(variable_image,Consts.Data.Game_event.Variable.Variable_image_state.shown);
					
					fading_images.Add (variable_image);
					
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_in,fade_mode,fade_time);

				
				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.hide){
					
					
					
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.hidden);
					
					state=Consts.Data.Scene_settings.Action.Image_operation_state.image_operation_finished;
					
					
				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.fade_out){
					
					
					
					state=Consts.Data.Scene_settings.Action.Image_operation_state.running_image_operation;
					
					notify_me_in_condition(variable_image,Consts.Data.Game_event.Variable.Variable_image_state.hidden);
					
					fading_images.Add (variable_image);
					
					variable_image.change_state(Consts.Data.Game_event.Variable.Variable_image_state.start_fade_out,fade_mode,fade_time);
					
					
					
					
				}else if(operation==Consts.Data.Scene_settings.Action.Image_operation.change){
					
					state=Consts.Data.Scene_settings.Action.Image_operation_state.image_operation_finished;
					
					
					//variable_image.change_texture();
					
					
				}
			}

			

			
			if (state==Consts.Data.Scene_settings.Action.Image_operation_state.running_image_operation){
				
				
				
			}


			if (state==Consts.Data.Scene_settings.Action.Image_operation_state.image_operation_finished){
				
				is_finished=true;
				
			}
			

			
			if(is_finished){

				state=Consts.Data.Scene_settings.Action.Image_operation_state.none;
				
				Destroy(created_game_event_list);
				
				if(parent_action_list!=null){
					parent_action_list.run_next ();
				}
			}
			
		
		}

		
		public override void variable_changed(Variable input_variable,Game_event input_game_event){

			
			if(state==Consts.Data.Scene_settings.Action.Image_operation_state.running_image_operation){
				
				state=Consts.Data.Scene_settings.Action.Image_operation_state.image_operation_finished;
				
				Variable_image temp= (Variable_image) input_variable;
				
				fading_images.Remove (temp);
				created_game_event_list.remove_game_event (input_game_event);
				
				run_internal(true);
				
			}
		}
		
		public override void child_action_finished(Action input_child_action){
			
			print (state);
			
			if(input_child_action==child_action){
				/*
				if(state==Consts.Data.Scene_settings.Action.Person_image_operation_state.showing_text){
					state=Consts.Data.Scene_settings.Action.Person_image_operation_state.text_operation_finished;
					run_internal (true);
				}*/
			}
		}
		
		private void notify_me_in_condition(Variable_image input_variable_image,Consts.Data.Game_event.Variable.Variable_image_state input_state){
			
			if(created_game_event_list==null){
				created_game_event_list=gameObject.AddComponent<Game_event_list>();
			}
			
			
			
			Game_event game_event = created_game_event_list.create_game_event ();
			
			
			Condition_image condition =(Condition_image) game_event.add_condition<Condition_image>();
			
			condition.variable_image_state = input_state;
			
			condition.variable_image=input_variable_image;
			
			
			Notify_variable_changed notify =(Notify_variable_changed) game_event.add_action<Notify_variable_changed>();
			
			notify.initialize(this,input_variable_image);
			
			
			game_event.add_event_to_variables();
		}
		
	}
}
