﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.SCENE_SETTINGS;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	[ExecuteInEditMode]
	public class Jump_tag_list :MonoBehaviour {

		public bool search_all_tags = false;

		public List<string> jump_tag_list= new List<string>();

		public List<Action> action_list = new List<Action>();

		public List<Action_list> action_list_list= new List<Action_list>();


		//public SAKURA.Consts.Data.Scene_settings.Action.Jump_type jump_type;
		//public string destination;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){

				if(search_all_tags){

					clear_data();

					Action[] temp=  FindObjectsOfType(typeof(Action)) as Action[];

					foreach (Action a in temp){

						set_data(a);

					}
					search_all_tags=false;

				}

			}


		
		}

		public void clear_data(){

			jump_tag_list = new List<string> ();
			action_list = new List<Action> ();
			action_list_list = new List<Action_list> ();

			
		}


		public void set_data(Action input_action){

			if(input_action.tag!=""){

				jump_tag_list.Add (input_action.tag); 
				action_list.Add (input_action);
				action_list_list.Add (input_action.parent_action_list);
			}

		}

		public void jump(string input_tag){

			for (int i=0; i<jump_tag_list.Count; i++){

				if(jump_tag_list[i]==input_tag){

					action_list_list[i].run_at(action_list[i].action_list_order);

					break;
				}

			}

		}
	}
}
