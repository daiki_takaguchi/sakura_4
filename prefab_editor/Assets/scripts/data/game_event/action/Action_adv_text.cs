﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.UI.LAYER;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	public class Action_adv_text : Action {


		public SAKURA.Consts.Data.Scene_settings.Action.Person_name person_name;

		public string text;

		public string remaining_text;

		public SAKURA.Consts.Data.Scene_settings.Action.Text_state state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_run;

		[HideInInspector] public SAKURA.Consts.Ui.Objects.Touch_panel_state touch_panel_state;


		[HideInInspector] public bool is_long_pressed=false;

		[HideInInspector] public bool is_tapped=false;

		[HideInInspector] public bool is_run_finished=false;

		[HideInInspector] private Coroutine waiting_croutine;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {



		
		}


		IEnumerator run_again(float wait_time) {
			yield return new WaitForSeconds(wait_time);
			run_internal (true);
		}


		public override void run(Action input_action){

			parent_action = input_action;

			run_internal (false);
		}

		public override void run(Action_list input_action_list){

			parent_action_list = input_action_list;

			run_internal (false);

		}

		public void run_internal(bool is_internal){

			//Variable_touch_panel variable_touch_panel= (Variable_touch_panel) Gameplay.gameplay_static.get_variable(typeof(Variable_touch_panel));
			
			Adv_text_layer adv_text = (Adv_text_layer) Ui.ui_static.layer_list.get_layer (Consts.Ui.Layer_type.adv_text);

			if (state == Consts.Data.Scene_settings.Action.Text_state.waiting_for_run) {
				


				notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state.two_finger_long_press);

				notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state.one_finger_tap);

				/*
				if(!adv_text.is_shown()){
					SAKURA.Ui.ui_static.show (Consts.Ui.Layer_type.adv_text);
				}*/

				//adv_text.show ();

				SAKURA.Ui.ui_static.show_if_hidden (Consts.Ui.Layer_type.adv_text);
				

				
				//adv_text.person_name.set_text(SAKURA.DATA.Resources.resources_static.get_person_name (person_name),Consts.Ui.Objects.Text_mode.all_at_once,out temp);
				adv_text.set_name(person_name);
				
				//prev_run_time=Time.time;
				
				float text_speed=SAKURA.Data.data_static.player_data.get_text_speed();
				
				if(text_speed<=0){
					
					adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);
					
					state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button;
					
				}else{
					if(adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.type_writer,out remaining_text)){
						
						state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button;
					}else{
						waiting_croutine= StartCoroutine(run_again(text_speed));
						state=Consts.Data.Scene_settings.Action.Text_state.showing;
					}
					
					
				}
				
			}else if(state==Consts.Data.Scene_settings.Action.Text_state.showing){
				
				if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.none){
					if(adv_text.main_text.add_text (remaining_text,Consts.Ui.Objects.Text_mode.type_writer,out remaining_text)){

						state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button;
						
					}else{
						float text_speed=SAKURA.Data.data_static.player_data.get_text_speed();
						waiting_croutine =StartCoroutine(run_again(text_speed));
					};
					
				}else if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap){


					adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);

					state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button;
					
					touch_panel_state=Consts.Ui.Objects.Touch_panel_state.none;
					
				}else if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_long_press){
					adv_text.main_text.set_text (text,Consts.Ui.Objects.Text_mode.all_at_once,out remaining_text);
					
					state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button;
					
					is_long_pressed=true;

					waiting_croutine =StartCoroutine(run_again(0.5f));
				}
				
				
				
				
			}else if(state==Consts.Data.Scene_settings.Action.Text_state.waiting_for_next_button){

				print ("waiting");
				
				if(touch_panel_state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap
				   ||is_long_pressed){
					is_run_finished=true;


				}
			}
			
			if(is_run_finished){
					
				print ("text finished");

					created_game_event_list.destroy_all_game_events();
					
					Destroy(created_game_event_list);
					
					if(parent_action!=null){
						parent_action.child_action_finished(this);
						Destroy(this.gameObject);
						
					}
					
					if(parent_action_list!=null){
						parent_action_list.run_next();
					}



				state=Consts.Data.Scene_settings.Action.Text_state.waiting_for_run;
			
			}

		}

		public void initalize(SAKURA.Consts.Data.Scene_settings.Action.Person_name input_person_name,string input_text){
			person_name = input_person_name;
			text = input_text;
		
		}

		public override void variable_changed (Variable input_variable,Game_event input_game_event)
		{

	

			if(input_variable is Variable_touch_panel){


				Variable_touch_panel temp= (Variable_touch_panel) input_variable ;


				if(temp.state==Consts.Ui.Objects.Touch_panel_state.one_finger_long_press
				   ||temp.state==Consts.Ui.Objects.Touch_panel_state.one_finger_tap){

					print (state+" "+temp.state);

					touch_panel_state=temp.state;


					if(waiting_croutine!=null){

						StopCoroutine(waiting_croutine);
					}

					run_internal(true);
				}
			}
		}


		private void notify_me_in_condition(Consts.Ui.Objects.Touch_panel_state input_condition_state){
			if(created_game_event_list==null){
				created_game_event_list=gameObject.AddComponent<Game_event_list>();
			}


			Game_event game_event =created_game_event_list.create_game_event();
			
			Condition_touch_panel condition =(Condition_touch_panel) game_event.add_condition<Condition_touch_panel>();
			
			condition.condition_touch_panel_state=input_condition_state;
			
			Notify_variable_changed notify =(Notify_variable_changed) game_event.add_action<Notify_variable_changed>();
			
			notify.initialize(this,Gameplay.gameplay_static.get_variable (typeof(Variable_touch_panel)));
			
			game_event.add_event_to_variables();

			print (created_game_event_list.game_event_list.Count);



		}




	

	}



}
