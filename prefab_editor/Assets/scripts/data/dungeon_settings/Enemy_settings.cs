﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.LAYER;



namespace SAKURA.DATA.DUNGEON_SETTINGS{
	[ExecuteInEditMode]
	public class Enemy_settings : MonoBehaviour {

		public bool add_enemy_move_point=false;
		public bool refresh_points_now=false;

		public Dungeon_settings dungeon_settings;

		public List<DUNGEON_SETTINGS.Enemy_move_point> enemy_move_points;
		[HideInInspector] public List<SAKURA.UI.OBJECTS.Enemy_move_point_panel> enemy_move_points_panels = new List<SAKURA.UI.OBJECTS.Enemy_move_point_panel> ();
	
		[HideInInspector]  public Transform enemy_move_points_parent;
		
		//public Transform enemy_move_points_panels_parent;
		
		[HideInInspector] public GameObject enemy_move_point_prefab;
		
		[HideInInspector] public GameObject enemy_move_point_panel_prefab;

		public Enemy_move_point initial_move_point;
		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Mode initial_mode;
		public SAKURA.Consts.Gameplay.Dungeon.Enemy.Sub_mode initial_sub_mode;

		
		public float angle_of_view;
		public float range_of_view;
		public float view_swing_prob;
		public float view_swing_speed_in_seconds;
		//public float walk_prob;
		public float walk_turn_time;
		public float walk_length_in_one_turn;
		public float rotation_speed_in_seconds;
		public float approaching_prob;
		public float calm_down_seconds;





		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){
				if(add_enemy_move_point){
					
					GameObject temp= (GameObject) Instantiate(enemy_move_point_prefab);
					temp.transform.parent=enemy_move_points_parent;
					temp.name="point_"+enemy_move_points.Count;
						if(temp.GetComponent<Enemy_move_point>()!=null){
							temp.GetComponent<Enemy_move_point>().enemy_settings=this;
							enemy_move_points.Add (temp.GetComponent<Enemy_move_point>());
						}
					//dungeon_settings.refresh_points();



					add_enemy_move_point=false;
				}

				if(refresh_points_now){
					dungeon_settings.refresh_points();
					refresh_points_now=false;
				}
			}

		}

		public void set_points(){
			
			
			int count = 0;
			
			enemy_move_points = new List<DUNGEON_SETTINGS.Enemy_move_point> ();


			/*
			foreach(SAKURA.UI.OBJECTS.Enemy_move_point_panel panel in enemy_move_points_panels){
				if(panel!=null){
					DestroyImmediate (panel.gameObject);
				}
			}
			*/

			Layer  temp_layer = GameObject.FindObjectOfType<Ui>().layer_list.get_layer (Consts.Ui.Layer_type.enemy_move_points_panels);

			temp_layer.destroy_immediate_all_objects ();

			enemy_move_points_panels = new List<SAKURA.UI.OBJECTS.Enemy_move_point_panel> ();
			
			foreach (Transform child in enemy_move_points_parent)
			{
				
				DUNGEON_SETTINGS.Enemy_move_point point=child.GetComponent<DUNGEON_SETTINGS.Enemy_move_point>();
				
				if(point!=null){
					point.set_id (count);
					enemy_move_points.Add (point);
					point.gameObject.name="point_"+count;
					
					
					GameObject	temp= (GameObject) Instantiate(enemy_move_point_panel_prefab);
					temp.name="panel_"+count;


					temp_layer.add_object(temp);
					temp.transform.localScale= new Vector3(1,1,1);
					
					Vector3 screnn_position=GameObject.Find ("system").GetComponent<Sakura>().ui.world_camera.WorldToScreenPoint(point.transform.position);
					
					temp.transform.localPosition= new Vector3(screnn_position.x-Screen.width/2,screnn_position.y-Screen.height/2,0);
					
					SAKURA.UI.OBJECTS.Enemy_move_point_panel panel=temp.GetComponent<SAKURA.UI.OBJECTS.Enemy_move_point_panel>();
					panel.label.text=""+count;
					enemy_move_points_panels.Add (panel);

					count++;
				}
			}
		}

	}


}