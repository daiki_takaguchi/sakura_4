﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;

namespace SAKURA.DATA.SCENE_SETTINGS{

	[ExecuteInEditMode]
	public class Adv_settings_list : Adv_settings_base {

		public List<Chapter_settings> chapter_settings_list;


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){

				chapter_settings_list= Sakura.get_monos_from_children<Chapter_settings>(transform);

				variables_current_scope.variable_scope_type=variable_scope_type;
				variables_current_scope.gameObject.name="variable_list global ";

				foreach(Chapter_settings chapter in chapter_settings_list){

					chapter.gameObject.name="chapter "+chapter.chapter_no;
					


					int chapter_no=chapter.chapter_no;

					chapter.variables_current_scope.variable_scope_type=chapter.variable_scope_type;
					chapter.variables_current_scope.gameObject.name="variable_list "+chapter.gameObject.name;


					int scene_no=0;
					chapter.scene_settings=Sakura.get_monos_from_children<Scene_settings>(chapter.transform);

				

					foreach (Scene_settings scene in chapter.scene_settings){
						

						
						scene.chapter_no=chapter_no;
						
						scene.gameObject.name="scene "+scene.scene_name;
						

						scene.variables_current_scope.variable_scope_type=scene.variable_scope_type;
						scene.variables_current_scope.gameObject.name="variable_list "+scene.gameObject.name;

						scene_no++;

						int sequence_no=0;

						scene.scene_sequences=Sakura.get_monos_from_children<Scene_sequence>(scene.transform);
						
						foreach (Scene_sequence sequence in scene.scene_sequences){
							

							
							sequence.chapter_no=chapter.chapter_no;
							sequence.scene_name=scene.scene_name;
							//sequence.parallel_no=parallel_no;
							sequence.sequence_no=sequence_no;
							
							sequence.scene_settings=scene;
							
							sequence.gameObject.name="sequence "+sequence_no;

							sequence.variables_current_scope.variable_scope_type=sequence.variable_scope_type;
							sequence.variables_current_scope.gameObject.name="variable_list "+sequence.gameObject.name;


							sequence_no++;

							int state_no=0;

							sequence.states=Sakura.get_monos_from_children<Scene_sequence_state>(sequence.transform);
							
							foreach (Scene_sequence_state sequence_state in sequence.states){
								

								
								sequence_state.chapter_no=chapter.chapter_no;
								sequence_state.scene_name=scene.scene_name;

								sequence_state.sequence_no=sequence.sequence_no;
								sequence_state.state_no=state_no;
								
								sequence_state.scene_sequence=sequence;



								sequence_state.gameObject.name="state "+state_no;

								sequence_state.variables_current_scope.variable_scope_type=sequence_state.variable_scope_type;
								sequence_state.variables_current_scope.gameObject.name="variable_list "+sequence_state.gameObject.name;

								state_no++;

							

							}
							
							
						}

					}
				}
			}
			
		
			
		}
		


	}
}
