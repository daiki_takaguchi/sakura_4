using UnityEngine;
using System.Collections;

namespace SAKURA{
	public class Consts  {

		// Use this for initialization



		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public class Data{

			public class Game_settings{
				//public  const float grid_length=5.12f;
			}

			public enum Savedata_type{
				none,
				nornal_savedata,
				quick_savedata
			}

			public class Game_event{

				public class Condition{

					/*
					public enum Coidition_type{
						default_condition,
						none,

					}*/

					public enum Coidition_float_operator{
						none,
						equals,
						greater,
						smaller,
						gr_or_eq,
						sm_or_eq
					}
				}
				public class Variable{

					/*
					public enum Variable_type{
						none,
						touch_panel
					}*/

					public enum Variable_scope_type{
						none,
						temporary,
						global,
						chapter,
						scene,
						scene_sequence,
						scene_sequence_state,
						gameplay,
						system
					}

					public enum Variable_item_state{
						none,
						not_found,
						owned,
						abandoned
					}

					public enum Variable_image_state{
						none,
						shown,
						hidden,
						start_fade_in,
						fading_in,
						start_fade_out,
						fading_out

					}

					public enum Variable_object_position_initial_type{
						none,
						grid_local,
						grid_global,
						exact_local,
						exact_global

					}

					public enum Variable_object_position_state{
						none,
						animation_on,
						//animation_off,
						
					}

					public enum Variable_object_rotation_initial_type{
						none,
						local,
						global
						
					}

				

					public enum Variable_object_rotation_state{
						none,
						animation_on,
						//animation_off

						
					}
				}
			}


			public class Memo_data{
				public enum Memo_data_type{
					none,
					information,
					explanation
				}
			}
			public class Scene_settings{
				public class Action{
					/*
					public enum Action_type{
						none,
						change_person_image,
						change_background,
						change_game_state,
						change_layer_state,
						change_adv_text,
						play_audio,
						jump_to_scene,
						effect,
						unlock_memo,
						wait,

						filter,
						shake,
						beat,
						blink,
						select,

						scene
					}*/

					public enum Jump_type{
						none,
						scene,
						tag,
						title
					}


					public enum Effect_type{
						none,
						beat,
						night,
						clear
					}

					public enum Move_object_operation{
						none,
						check_collision_and_move_forward_animation,
						check_collision_and_move_forward_animation_loop,
						rotate_left_animation,
						rotate_right_animation
					}

					public enum Image_operation{
						none,
						show,
						fade_in,
						hide,
						fade_out,
						change,
						move
					}

					public enum Image_operation_state{
						none,
						waiting_for_run,
						running_image_operation,
						image_operation_finished
					}

					public enum layer_operation{
						none,
						show,
						hide
					}


					public enum Fade_mode{
						none,
						alpha
					}

				

					public enum Person_image_position{
						none,
						C,
						L,
						L2,
						R,
						R2
					}

					public enum Person_image_operation_state{
						none,
						waiting_for_run,
						hiding_previous_image,
						ready_to_show_image,
						running_image_operation,
						image_operation_finished,
						creating_text,
						showing_text,
						text_operation_finished
					}


					public enum Person_name{
						none,
						a,
						b

					}

					public enum Person_image_expression{
						none,
						normal,
						akire
					}



					public enum Text_state{
						none,
						waiting_for_run,
						showing,
						waiting_for_next_button
					}
				}
			}


		}

		public class Audio{
			public class Se_loop{
				public enum Se_loop_type{
					beat,
					footsteps
				}
			}
		}

		public class Gameplay{
			public enum Game_state_type{
				title,
				adv,
				dungeon_test,
				backlog
			}

			public class Dungeon{

				public enum Dangeon_state_type{
					none,
					enemy_test
				}

				public class Enemy{
					
					public enum Mode{
						none,
						investigate,
						caution,
						found,
						caution_test,
						caution_started_test,

					}

					public enum Sub_mode{
						none,
						rotate,
						swing
					}

					public enum Rotation_direction{
						none,
						left,
						right
					}

					public enum Swing_mode{
						none,
						right,
						left,
						front
					}
				}
			}
		}

		public class Ui{
			public enum Layer_type{
				none,
				title,
				adv,
				adv_text,
				dungeon,
				menu,
				dungeon_menu,
				flowchart,
				memo,
				backlog,
				config,
				manual,
				enemy_move_points_panels
			}

			public enum Layer_depth_type{
				none,
				touch_panel,
				title_background,
				title_buttons,
				title_buttons_label,
				adv_background,
				adv_C,
				adv_L,
				adv_L2,
				adv_R,
				adv_R2,
				adv_buttons,
				adv_texts_background,
				adv_texts_button,
				adv_texts_texts,

				touch_panel_debug
			}


			/*
			public Layer_depth_type[] Layer_depth =new Layer_depth_type[]{
				Layer_depth_type.title_background,
				Layer_depth_type.title_buttons
			};*/

			public enum Adv_layer_image_type{
				none,
				background,
				still_1,
				still_2,
				still_3,
				still_4,
				C,
				L,
				L2,
				R,
				R2
			}

			/*
			public enum Adv_layer_state{
				two_people,
				three_people
			}*/

			public class Objects{

				/*
				public enum Image_state{
					shown,
					hidden,
					fade_in,
					fade_out
				}*/


				public enum Text_mode{
					none,
					all_at_once,
					type_writer
				}

				public enum Text_vertical_align{
					none,
					top,
					middle,
					bottom
				}

				public enum finger_state{
					none,
					press_started,
					pressing,
					unpressed,
				}

				public enum Touch_panel_state{
					none,
					one_finger_press_started,
					two_finger_press_started,
					one_finger_long_press,
					two_finger_long_press,
					one_finger_tap,
					two_finger_tap,
					swipe_up,
					swipe_down,
					swipe_left,
					swipe_right
				}
			}
		}

		public class Unity_editor{
			public class Adjust_transform_position{
				public enum Auto_adjust_mode{
					local_position,
					global_position,
					none
				}
			}

			public class Csv_to_action_conversion{
				public enum Variable_type{
					none,
				
					string_type,
					float_type,
					person_name_type,

				}

				public enum column_type{
					none,
					Command,
					Name,
					Text,
					para1,
					para2,
					para3,
					X,
					Y,
					@Time,
					No
				}
			}

			public class Object_prefab{

				public enum Data_type{
					prefab_GUID,

					prefab_type,

					object_name,

					local_position,

					local_rotation,

					local_scale,

					grid_length,
				}

				public enum Prefab_type{
					map,
					object_in_map,
					custom_grid,
					none,
				}
			}
		}

	}
	
}
