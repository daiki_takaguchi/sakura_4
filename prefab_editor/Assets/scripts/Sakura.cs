﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SAKURA;


public partial class Sakura: MonoBehaviour {


	public Data data;
	public Ui ui;
	public Gameplay gameplay;
	public Debug_settings debug_settings;


	void Start () {
		//print (normalize_angle(-460));
	}

	// Update is called once per frame
	void Update () {

	}


	public static Vector2 V3_to_V2_ignore_z(Vector3 input_vector){
		return new Vector2 (input_vector.x,input_vector.y);
	}

	public static Vector3 V2_to_V3_ignore_z(Vector2 input_vector){
		return new Vector3 (input_vector.x,input_vector.y,0);
	}

	public static Vector3 V3_ignore_z(Vector3 input_vector){
		return V2_to_V3_ignore_z (V3_to_V2_ignore_z (input_vector));
	}

	public static Quaternion from_to_rotation_z(Vector3 from, Vector3 to){

		//print (from);
		//print (to);

		Quaternion output= Quaternion.identity;

		Vector3 from_normalized = V3_ignore_z(from).normalized;
		Vector3 to_normalized = V3_ignore_z(to).normalized;


		if((from_normalized.sqrMagnitude!=0)&&(to_normalized.sqrMagnitude!=0)){

			if((from_normalized+to_normalized).sqrMagnitude!=0){
				output= Quaternion.Euler(new Vector3(0,0,Quaternion.FromToRotation(from_normalized,to_normalized).eulerAngles.z));

			}else{
				output= Quaternion.Euler(new Vector3(0,0,180));
			}

		}


		
		return output;
	}


	public static Vector2 V3_to_V2_ignore_y(Vector3 input_vector){
		return new Vector2 (input_vector.x,input_vector.z);
	}
	
	public static Vector3 V2_to_V3_ignore_y(Vector2 input_vector){
		return new Vector3 (input_vector.x,0,input_vector.y);
	}
	
	public static Vector3 V3_ignore_y(Vector3 input_vector){
		return V2_to_V3_ignore_y (V3_to_V2_ignore_y (input_vector));
	}
	
	public static Quaternion from_to_rotation_y(Vector3 from, Vector3 to){
		
		//print (from);
		//print (to);
		
		Quaternion output= Quaternion.identity;
		
		Vector3 from_normalized = V3_ignore_y(from).normalized;
		Vector3 to_normalized = V3_ignore_y(to).normalized;
		
		
		if((from_normalized.sqrMagnitude!=0)&&(to_normalized.sqrMagnitude!=0)){
			
			if((from_normalized+to_normalized).sqrMagnitude!=0){


				output= Quaternion.Euler(new Vector3(0,Quaternion.FromToRotation(from_normalized,to_normalized).eulerAngles.y,0));

			}else{
				output= Quaternion.Euler(new Vector3(0,180,0));
			}
			
		}
		
		
		
		return output;
	}

	public static float normalize_angle(float input_angle){

		float output = 0;
		if (input_angle > 0) {
			output=input_angle%360.0f;
		}else if(input_angle <0){
			output=input_angle%360.0f +360.0f;
		}

		return output;
	}
	 
	public static List<TOutput> get_monos_from_children<TOutput> (Transform input_transform) where TOutput:MonoBehaviour  {

		List<TOutput> output = new List<TOutput>();

		foreach (Transform child in input_transform){
			TOutput temp=(TOutput) child.GetComponent (typeof(TOutput));

			if(temp!=null){
				output.Add(temp);
			}
		}
		return output;
	}


	public static TOutput get_mono_from_children<TOutput> (Transform input_transform) where TOutput:MonoBehaviour  {

		TOutput output = null;

		foreach (Transform child in input_transform){
			TOutput temp=(TOutput) child.GetComponent (typeof(TOutput));
			
			if(temp!=null){
				output=temp;
				break;
			}
		}

		return output;
	}

	public static List<TOutput> remove_null<TOutput> (List<TOutput> input_list) where TOutput:MonoBehaviour  {
		
		List<TOutput> output = new List<TOutput>();
		
		foreach (TOutput input in input_list){

			if(input!=null){
				output.Add(input);
			}
		}
		return output;
	}


	public static Vector2 screen_position_to_local_position(Vector2 input_position){
		
		return new Vector2 ((input_position.x - (Screen.width / 2)) * (640.0f / Screen.width), (input_position.y - (Screen.height / 2)) * (640.0f / Screen.width));
	}

	public static bool try_get_touched_position(out Vector2 output_position){
		Vector2 input_position= new Vector2(0,0);
		
		bool output=false;
		
		if (Input.touchCount>0) {
			output=true;
			input_position=  Input.GetTouch(0).position;
		} 
		
		if(Input.GetMouseButton(0)){
			output=true;
			input_position=  new Vector2 (Input.mousePosition.x,Input.mousePosition.y);
		}
		
		
		output_position=Sakura.screen_position_to_local_position(input_position);
		
		return output;
	}

	public static Vector3 mutiply_for_each(Vector3 input_vector_1,Vector3 input_vector_2){

		return new Vector3 (input_vector_1.x*input_vector_2.x,input_vector_1.y*input_vector_2.y,input_vector_1.z*input_vector_2.z);
	}

	public static Vector3 mfe(Vector3 input_vector_1,Vector3 input_vector_2){
		return mutiply_for_each (input_vector_1,input_vector_2);
	}




	public static Vector3 closest_six_axis(Vector3 input_vector){
		Vector3 output = new Vector3 (0,0,0);

		float inner=0;

		List<Vector3> axis_list = new List<Vector3> (); 

		axis_list.Add (Vector3.up);
		axis_list.Add (Vector3.down);
		axis_list.Add (Vector3.left);
		axis_list.Add (Vector3.right);
		axis_list.Add (Vector3.forward);
		axis_list.Add (Vector3.back);

		foreach(Vector3 axis in axis_list){

			float temp=Vector3.Dot(axis,input_vector);

			if(temp>inner){
				inner=temp;
				output=axis;
			}
		}


		return output;
		
	}


	public static Vector3 closest_grid_point(Vector3 input_vector, Vector3 input_grid){

		float x = closest_grid_point (input_vector.x,input_grid.x);
		float y = closest_grid_point (input_vector.y,input_grid.y);
		float z = closest_grid_point (input_vector.z,input_grid.z);

		return  new Vector3(x,y,z);
	}

	public static float closest_grid_point(float input_float, float input_grid){

		return Mathf.Round (input_float / input_grid) * input_grid;
	}

	public static List<float> vector3_to_floats(Vector3 input_vector){
		
		List<float> output = new List<float> ();
		
		output.Add (input_vector.x);
		output.Add (input_vector.y);
		output.Add (input_vector.z);
		
		return output;
		
	}

	public static Vector3 floats_to_vector3(List<float> input_floats){

		Vector3 output = new Vector3 (0,0,0);

		if(input_floats.Count==3){

			output= new Vector3(input_floats[0],input_floats[1],input_floats[2]);

		}else{
			Debug.LogError("floats count does not match!");
		}
		
		return output;
		
	}

	
	public static string get_variable_value<T_input>(T_input input_instance ,Type action_type,string variable_name, Type value_type){

		string output = "";

		//print (typeof (T_input).ToString());

		//print (variable_name+" type:"+action_type.Name);

		System.Reflection.FieldInfo field = input_instance.GetType().GetField (variable_name);

		//print (input_instance.GetType().Name);

		if (field != null) {


			output = field.GetValue(input_instance).ToString ();

			//print (output);

		} else {

			Debug.LogError("conversion failed!");
		}

		//print (output);

		return output;
		
	}

	public static T_input set_variable_value<T_input>(T_input input_instance,Type action_type,string variable_name,Type input_type,string input_value ){
		

		System.Reflection.FieldInfo field = input_instance.GetType().GetField (variable_name);


		if (field != null) {

			field.SetValue(input_instance,input_value);
		} else {
			
			Debug.LogError("conversion failed!");
		}

		
		return input_instance;
		
	}





}



