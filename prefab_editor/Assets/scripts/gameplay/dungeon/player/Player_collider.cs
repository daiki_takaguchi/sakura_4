﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.GAMEPLAY.DUNGEON{
	public class Player_collider : MonoBehaviour {

	// Use this for initialization

		public Player player;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		void OnTriggerEnter(Collider hit) {


			SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point point = hit.GetComponent<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point> ();
			
			if(point!=null){
				print ("player is in id " +point.id);
				player.current_points.Add(hit.GetComponent<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point>());
			}
		}

		void OnTriggerExit(Collider hit) {

			List<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point> new_points = new List<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point> ();

			foreach (SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point point in player.current_points){
				if(point!=hit.GetComponent<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point> ()){
					print ("player is out of id " +point.id);
					new_points.Add (point);
				}
			}

			player.current_points = new_points;



		}

	}
}