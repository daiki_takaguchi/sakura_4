﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.GAMEPLAY.DUNGEON;
using SAKURA.DATA;
using SAKURA.DATA.DUNGEON_SETTINGS;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.ACTION;


namespace SAKURA.GAMEPLAY{
	public class Dungeon : MonoBehaviour {

		public Consts.Gameplay.Dungeon.Dangeon_state_type dungeon_state;

		public SAKURA.DATA.Dungeon_settings dungeon_settings;

		public Game_event_list game_event_list;

		public Gameplay gameplay;

		public Player player;

		public List<Enemy> enemies;

		//public Camera world_camera;

		///public Transform hit_transform;

		//public GameObject warning;

		//public GameObject dungeon_layer;

		public Transform dungeon_objects;

		public GameObject enemy_object_prefab;


		// Use this for initialization
		void Start () {


		}

		public void set_dungeon_settings(Dungeon_settings input_dungeon_settings){

		}

		public void change_dungeon_state (SAKURA.Consts.Gameplay.Dungeon.Dangeon_state_type input_state){
			dungeon_state = input_state;

			if(input_state==Consts.Gameplay.Dungeon.Dangeon_state_type.enemy_test){

				enable_player_move();

				foreach(Enemy enemy in enemies){
					enemy.destroy_enemy();
				}

				enemies= new List<Enemy>();

				int enemy_count=0;

				foreach(Enemy_settings enemy_settings in dungeon_settings.enemy_settings){

					Enemy enemy= create_enemy();

					enemy.gameObject.name="enemy "+enemy_count;

					enemy.enemy_object.gameObject.name="enemy "+enemy_count;

					enemy.initialize (enemy_settings);

					enemies.Add (enemy);

					enemy_count++;

				}
			
			}
		}

		public Enemy create_enemy(){

			GameObject enemy_gameobject = new GameObject();

			Enemy enemy= enemy_gameobject.AddComponent <Enemy>();
			
			enemy.transform.parent=transform;

			GameObject enemy_object_gameobject= (GameObject) Instantiate(enemy_object_prefab);

			enemy_object_gameobject.transform.parent= dungeon_objects;

			enemy.enemy_object= enemy_object_gameobject.GetComponent<Enemy_object>();
			


			return enemy;
	

		}

		// Update is called once per frame
		void Update () {

			if(dungeon_state==Consts.Gameplay.Dungeon.Dangeon_state_type.enemy_test){

				/*
				if(Input.GetKey (KeyCode.LeftArrow)){
					player.rotation.Rotate(new Vector3(0,-5,0));

				}else if(Input.GetKey (KeyCode.RightArrow)){

					player.rotation.Rotate(new Vector3(0,5,0));
				}

				if(Input.GetKey (KeyCode.UpArrow)){
					player.position.localPosition+=player.rotation.localRotation*new Vector3(0,0,0.2f);
					
				}else if(Input.GetKey (KeyCode.DownArrow)){
					
					player.position.localPosition+=player.rotation.localRotation*new Vector3(0,0,-0.2f);
				}*/




				play_footsteps ();

			}

		}

		void FixedUpdate ()
		{

			bool is_spotted = false;;


			foreach(Enemy enemy in enemies){
				if(is_user_in_enemy_view(enemy)){

					RaycastHit hit;

					Vector3 enemy_position=enemy.get_local_position();
					Vector3 player_position=player.get_local_position();

					if (Physics.Raycast ( enemy_position,player_position - enemy_position, out hit)) {

						//hit_transform=hit.transform;

						if(hit.transform==player.player_object.transform){
							is_spotted=true;
						}

						//print (hit.transform.name);
					}
				}
			}

			//print (angle_from_enemy_to_user(enemy));

			//warning.SetActive (is_spotted);
		}


	
		void play_footsteps(){

			/*
			SAKURA.AUDIO.Se_loop se_loop = SAKURA.Audio.audio_static.footsteps;

			float pan = angle_from_user_to_enemy(enemy) / 90.0f;

			if(pan>1){
				pan=2-pan;
			}else if(pan<-1){
				pan=-2-pan;
			}

			//se_loop.change_pan (pan);

			float volume= 1-(distance_between_user_and_enemy()/enemy_settings.auditory_radius);

			if(volume<0){
				volume=0;
			}*/

			//se_loop.change_volume (volume);

			
		}

		float angle_from_enemy_to_user(DUNGEON.Enemy input_enemy){
		
			Vector3 enemy_front = input_enemy.get_local_rotation() *new Vector3 (0,0,1);

			Vector3 temp=Sakura.from_to_rotation_y (enemy_front,player.get_local_position()-input_enemy.get_local_position()).eulerAngles;
			
			float angle = temp.y;
			
			angle = angle % 360;
			
			if(angle>=180){
				angle=angle-360.0f;
			}

			return angle;
		}

		public bool is_user_in_enemy_view(DUNGEON.Enemy input_enemy){

			bool output = false;

			float limit_angle = 30.0f/2.0f;

			if(Mathf.Abs (angle_from_enemy_to_user(input_enemy))<=limit_angle){
				output=true;
			}

			return output;
		}

		float angle_from_user_to_enemy(DUNGEON.Enemy input_enemy){
			
			Vector3 user_front = player.get_local_rotation() *new Vector3 (0,0,1);
			
			Vector3 temp=Sakura.from_to_rotation_y(user_front,input_enemy.get_local_position()-player.get_local_position()).eulerAngles;
			
			float angle = temp.y;
			
			angle = angle % 360;
			
			if(angle>=180){
				angle=angle-360.0f;
			}
			
			return angle;
		}

		float distance_between_user_and_enemy(Enemy input_enemy){

			return (player.get_global_position()-input_enemy.get_global_position()).magnitude;
		}


		public void enable_player_move(){

			Variable_object_position position=	gameplay.variable_list_gameplay.create_variable <Variable_object_position>("variable player position");

			position.object_position_transform= player.player_object.position_transform;

			position.refresh_children ();

			Variable_object_rotation rotation =gameplay.variable_list_gameplay.create_variable <Variable_object_rotation>("variable player rotation");
		
			rotation.object_rotation_transform = player.player_object.rotation_transform;

			rotation.refresh_children ();


			set_player_position_game_event (position, rotation);

			set_player_rotation_game_event (position, rotation, Consts.Ui.Objects.Touch_panel_state.swipe_left,Consts.Data.Scene_settings.Action.Move_object_operation.rotate_left_animation );

			set_player_rotation_game_event (position, rotation, Consts.Ui.Objects.Touch_panel_state.swipe_right, Consts.Data.Scene_settings.Action.Move_object_operation.rotate_right_animation);




		}

		public void set_player_position_game_event(Variable_object_position position, Variable_object_rotation rotation){

			Game_event game_event =game_event_list.create_game_event();
			
			Condition_touch_panel condition =(Condition_touch_panel) game_event.add_condition<Condition_touch_panel>();
			
			condition.condition_touch_panel_state = Consts.Ui.Objects.Touch_panel_state.one_finger_tap;

			Move_object move_object =(Move_object) game_event.add_action<Move_object>();
			
			move_object.initalize (position, rotation);
			
			move_object.operation = Consts.Data.Scene_settings.Action.Move_object_operation.check_collision_and_move_forward_animation;


			move_object.animation_end_relative_time = Data.data_static.game_settings.player_step_finish_time_from_start;
			
			game_event.add_event_to_variables ();






			Game_event game_event_2 =game_event_list.create_game_event();
			
			Condition_touch_panel condition_2 =(Condition_touch_panel) game_event_2.add_condition<Condition_touch_panel>();

			condition_2.condition_touch_panel_state = Consts.Ui.Objects.Touch_panel_state.one_finger_long_press;
			
			
			Move_object move_object_2 =(Move_object) game_event_2.add_action<Move_object>();
			
			move_object_2.initalize (position, rotation);
			
			move_object_2.operation = Consts.Data.Scene_settings.Action.Move_object_operation.check_collision_and_move_forward_animation_loop;
			
			
			move_object_2.animation_end_relative_time = Data.data_static.game_settings.player_step_finish_time_from_start;
			
			game_event_2.add_event_to_variables ();







			Game_event game_event_3 =game_event_list.create_game_event();
			
			Condition_touch_panel condition_3 =(Condition_touch_panel) game_event_3.add_condition<Condition_touch_panel>();
			
			condition_3.condition_touch_panel_state = Consts.Ui.Objects.Touch_panel_state.none;

			
			Notify_variable_changed notify =(Notify_variable_changed) game_event_3.add_action<Notify_variable_changed>();
			
			notify.initialize (move_object_2, gameplay.get_variable (typeof(Variable_touch_panel)));

		
			game_event_3.add_event_to_variables ();


		



		}

		public void set_player_rotation_game_event(Variable_object_position position, 
		                                           Variable_object_rotation rotation, 
		                                           Consts.Ui.Objects.Touch_panel_state input_state,
		                                           SAKURA.Consts.Data.Scene_settings.Action.Move_object_operation input_operation){

			Game_event game_event =game_event_list.create_game_event();
			
			Condition_touch_panel condition =(Condition_touch_panel) game_event.add_condition<Condition_touch_panel>();
			
			condition.condition_touch_panel_state = input_state;
			
			
			Move_object move_object =(Move_object) game_event.add_action<Move_object>();
			
			move_object.initalize (position, rotation);
			
			move_object.operation = input_operation;


			move_object.animation_end_relative_time = Data.data_static.game_settings.player_rotation_finish_time_from_start;
			
			game_event.add_event_to_variables ();
		}





		public void disable_player_move(){
			/*
			gameplay.variable_list_gameplay. <Variable_object_position>("variable player position");
			gameplay.variable_list_gameplay.create_variable <Variable_object_rotation>("variable player rotation");*/
		}





		//public void ch
	}
}
