﻿using UnityEngine;
using System;
using System.Collections;

namespace SAKURA.UI.NGUI{
	[ExecuteInEditMode]
	public class NGUI_depth : MonoBehaviour {

		//public bool save_layer=false;

		[HideInInspector]public SAKURA.Consts.Ui.Layer_depth_type prev_layer_depth_type;
		public SAKURA.Consts.Ui.Layer_depth_type layer_depth_type;
		//[HideInInspector] public bool is_layer_saved=false;

		[HideInInspector] public string layer_depth_type_string;
	

		// Use this for initialization
		void Start () {

			/*
			if(layer_depth_type!=(SAKURA.Consts.Ui.Layer_depth_type) Enum.Parse(typeof(SAKURA.Consts.Ui.Layer_depth_type), layer_depth_type_string)){
				Debug.LogError("layer error! name:"+gameObject.name +" layer: "+layer_depth_type+ "is not saved");

			}*/
		
			Vector3 temp = transform.localPosition;

			transform.localPosition = new Vector3 (temp.x,temp.y,-1*((int) layer_depth_type));



			
		}
		
		// Update is called once per frame
		void Update () {

			if(!Application.isPlaying){

				if(prev_layer_depth_type!=layer_depth_type){
					save_layer_depth_type();
				}else{

					string temp=layer_depth_type.ToString ();

					if(layer_depth_type_string!=temp){

						try{
							layer_depth_type=(SAKURA.Consts.Ui.Layer_depth_type) Enum.Parse(typeof(SAKURA.Consts.Ui.Layer_depth_type), layer_depth_type_string);
						
						}catch(ArgumentException) {
							Debug.LogError("layer error! name:"+gameObject.name +" layer: "+layer_depth_type_string+ "layer name is changed");
						}
					}
				}
		
			}
		}

		public void save_layer_depth_type(){
			prev_layer_depth_type = layer_depth_type;
			layer_depth_type_string = layer_depth_type.ToString ();
		}



	}
}
