﻿using UnityEngine;
using System.Collections;

namespace SAKURA.UI.NGUI {
	public class NGUI_scale : MonoBehaviour {

		// Use this for initialization

		//public float width;
		public float height;

		void Start () {
			Vector3 temp = transform.localScale;
			float scale = Screen.height / height;

			transform.localScale= new Vector3(temp.x*scale,temp.y*scale,1);
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}
