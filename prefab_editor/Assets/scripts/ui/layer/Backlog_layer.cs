﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.OBJECTS;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.UI.LAYER{
	public class Backlog_layer : Layer {

		// Use this for initialization

		public Variable_backlog variable_backlog;
		public Text text;
		public Scroll scroll;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public override List<SAKURA.Consts.Ui.Layer_type> show(){
			
			gameObject.SetActive (true);

			load_backlog ();

			scroll.initialize ();
			
			return layers_conflicting;
		}

		public void load_backlog(){

			string remaining = "";

			//print ("load_backlog");

			text.set_text (variable_backlog.current_value,Consts.Ui.Objects.Text_mode.all_at_once,out remaining);


		

		}


	
	}
	
}
