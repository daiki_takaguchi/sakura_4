﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.UI.LAYER{
	public class Adv_layer : Layer {

		// Use this for initialization

		[HideInInspector] public Variable_image variable_adv_background;
		[HideInInspector] public Variable_image variable_C;
		[HideInInspector] public Variable_image variable_L;
		[HideInInspector] public Variable_image variable_L2;
		[HideInInspector] public Variable_image variable_R;
		[HideInInspector] public Variable_image variable_R2;


		public Variable get_variable(SAKURA.Consts.Ui.Adv_layer_image_type image_type){
			return convert_image_type_to_variable(image_type);
		}

		public Variable convert_image_type_to_variable(SAKURA.Consts.Ui.Adv_layer_image_type image_type){
		
			Variable output = null;


			return output;
		}

		public Variable get_variable(SAKURA.Consts.Data.Scene_settings.Action.Person_image_position input_position){
			return convert_position_to_variable(input_position);
		}

		public Variable convert_position_to_variable(SAKURA.Consts.Data.Scene_settings.Action.Person_image_position input_position){

			Variable output = null;

			if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.C) {
				output = variable_C;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L) {
				output = variable_L;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R) {
				output = variable_R;
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L2) {
				output = variable_L2;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R2) {
				output = variable_R2;		
			}

			return output;


		}
		            

		/*public OBJECTS.Image background;
		public OBJECTS.Image C;
		public OBJECTS.Image L;
		public OBJECTS.Image L2;
		public OBJECTS.Image R;
		public OBJECTS.Image R2;/


		//private List<List<OBJECTS.Image>> fading_images= new List<List<SAKURA.UI.OBJECTS.Image>>();

		//public List<OBJECTS.Fading_images> fading_images = new List<OBJECTS.Fading_images>();

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}
*/
		public override List<SAKURA.Consts.Ui.Layer_type> show(){

			gameObject.SetActive (true);

			variable_adv_background.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);
			variable_C.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);
			variable_L.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);
			variable_L2.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);
			variable_R.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);
			variable_R2.change_state (Consts.Data.Game_event.Variable.Variable_image_state.hidden);


			return layers_conflicting;
		}
		/*
		public override List<SAKURA.Consts.Ui.Layer_type> show(List<> input_list){
		}*/


	/*
		public List<SAKURA.Consts.Ui.Layer_type> show(List<SAKURA.Consts.Data.Scene_settings.Action.Person_image_position> input_positions){

			return show (convert_to_image_type (input_positions));
		}

		public List<SAKURA.Consts.Ui.Layer_type> show(List<SAKURA.Consts.Ui.Adv_layer_image_type> input_list){

			foreach (SAKURA.Consts.Ui.Adv_layer_image_type image in input_list ){
				get_image(image).show();
			}
			
			return layers_conflicting;
		}
*/


		/*

		public List<SAKURA.Consts.Ui.Layer_type> start_fade_in(
			List<SAKURA.Consts.Data.Scene_settings.Action.Person_image_position> input_positions,
			SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode,
			float input_fade_time,
			Action input_action
			){

			
			return start_fade_in (convert_to_image_type (input_positions), input_fade_mode, input_fade_time, input_action);
		}

		public List<SAKURA.Consts.Ui.Layer_type> start_fade_in(
			List<SAKURA.Consts.Ui.Adv_layer_image_type> input_list,
			SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode,
			float input_fade_time,
			Action input_action
		){
			OBJECTS.Fading_images temp_fading_images=gameObject.AddComponent<OBJECTS.Fading_images>();

			List<OBJECTS.Image> temp_image_list = get_images (input_list);

			temp_fading_images.initialize (temp_image_list, input_action, this);

			fading_images.Add (temp_fading_images);

			foreach(OBJECTS.Image image in temp_image_list){
				image.start_fade_in(input_fade_mode,input_fade_time,temp_fading_images);
			}


			return layers_conflicting;
		}*/



	/*
		public void hide(List<SAKURA.Consts.Data.Scene_settings.Action.Person_image_position> input_positions){
			
			hide (convert_to_image_type (input_positions));
		}*/

		/*
		public void hide(List<SAKURA.Consts.Ui.Adv_layer_image_type> input_list){
			
			foreach (SAKURA.Consts.Ui.Adv_layer_image_type image in input_list ){
				get_image(image).hide();
			}

		}*/
		/*
		public List<SAKURA.Consts.Ui.Layer_type> start_fade_out(
			List<SAKURA.Consts.Data.Scene_settings.Action.Person_image_position> input_positions,
			SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode,
			float input_fade_time,
			Action input_action
			){
			
			
			return start_fade_out (convert_to_image_type (input_positions), input_fade_mode, input_fade_time, input_action);
		}
		
		public List<SAKURA.Consts.Ui.Layer_type> start_fade_out(
			List<SAKURA.Consts.Ui.Adv_layer_image_type> input_list,
			SAKURA.Consts.Data.Scene_settings.Action.Fade_mode input_fade_mode,
			float input_fade_time,
			Action input_action
			){

			OBJECTS.Fading_images temp_fading_images=gameObject.AddComponent<OBJECTS.Fading_images>();
			
			List<OBJECTS.Image> temp_image_list = get_images (input_list);
			List<OBJECTS.Image> new_temp_image_list = new List<OBJECTS.Image> ();

			foreach(OBJECTS.Image image in temp_image_list){
				if(image.is_shown()){
					new_temp_image_list.Add (image);
				}
			}

			temp_image_list = new_temp_image_list;

			temp_fading_images.initialize (temp_image_list, input_action, this);


			fading_images.Add (temp_fading_images);
			
			foreach(OBJECTS.Image image in temp_image_list){
				image.start_fade_out(input_fade_mode,input_fade_time,temp_fading_images);
			}
			
			
			return layers_conflicting;
		}

*/
		/*
		public void change_image_texture(Consts.Data.Scene_settings.Action.Person_image_position input_position,Texture input_texture){

			change_image_texture (convert_to_image_type (input_position), input_texture);
		}

		public void change_image_texture(Consts.Ui.Adv_layer_image_type input_image_type,Texture input_texture){


			change_image_texture (get_image (input_image_type).uitexture, input_texture);
		}

		private void change_image_texture(UITexture input_uitexture, Texture input_texture){

			input_uitexture.mainTexture = input_texture;
		}*/

		/*
		public bool is_people_image_shown(Consts.Data.Scene_settings.Action.Person_image_position input_position){
			return get_image_gameobject (convert_to_image_type(input_position)).activeSelf;
		}

		private UITexture get_image_uitexture(Consts.Ui.Adv_layer_image_type input_image_type){

			return get_image_gameobject(input_image_type).GetComponent<UITexture>();
		}*/
		/*
		private List<OBJECTS.Image> get_images(List<Consts.Ui.Adv_layer_image_type> input_image_types){

			List<OBJECTS.Image> output =new List<OBJECTS.Image>();

			foreach(Consts.Ui.Adv_layer_image_type input_image_type in input_image_types){
				output.Add(get_image (input_image_type));
			}

			return output;
		}*/
		/*
		private OBJECTS.Image get_image(Consts.Ui.Adv_layer_image_type input_image_type){


			OBJECTS.Image output = null;
			
			if(input_image_type==Consts.Ui.Adv_layer_image_type.background){
				output=background;
			}else if (input_image_type == Consts.Ui.Adv_layer_image_type.C) {
				output = C;
			}else if (input_image_type == Consts.Ui.Adv_layer_image_type.L) {
				output = L;
			}else if (input_image_type == Consts.Ui.Adv_layer_image_type.R) {
				output = R;
			}else if (input_image_type == Consts.Ui.Adv_layer_image_type.L2) {
				output = L2;
			}else if (input_image_type == Consts.Ui.Adv_layer_image_type.R2) {
				output = R2;
			}
			
			return output;



		}*/

		/*
		private GameObject get_image_gameobject(Consts.Ui.Adv_layer_image_type input_image_type){

			return get_image.;
		}*/
	/*
		private List<Consts.Ui.Adv_layer_image_type> convert_to_image_type(List<Consts.Data.Scene_settings.Action.Person_image_position> input_positions){
			List<SAKURA.Consts.Ui.Adv_layer_image_type> temp = new List<Consts.Ui.Adv_layer_image_type> ();
			
			foreach (SAKURA.Consts.Data.Scene_settings.Action.Person_image_position image in input_positions ){
				temp.Add (convert_to_image_type(image));
			}

			return temp;
		}

		private Consts.Ui.Adv_layer_image_type convert_to_image_type(Consts.Data.Scene_settings.Action.Person_image_position input_position){

			Consts.Ui.Adv_layer_image_type output = Consts.Ui.Adv_layer_image_type.C;

			if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.C) {
				output = Consts.Ui.Adv_layer_image_type.C;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L) {
				output = Consts.Ui.Adv_layer_image_type.L;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R) {
				output = Consts.Ui.Adv_layer_image_type.R;
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L2) {
				output = Consts.Ui.Adv_layer_image_type.L2;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R2) {
				output = Consts.Ui.Adv_layer_image_type.R2;		
			}

			return output;
		}*/

		/*
		private GameObject get_people_image_gameobject(Consts.Data.Scene_settings.Action.Person_image_position input_position){

			GameObject output = null;


			if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.C) {
				output = C;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L) {
				output = L;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R) {
				output = R;
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.L2) {
				output = L2;		
			}else if (input_position == Consts.Data.Scene_settings.Action.Person_image_position.R2) {
				output = R2;		
			}

			return output;

		}*/
		/*
		public bool is_shown(SAKURA.Consts.Data.Scene_settings.Action.Person_image_position input_position){
			return is_shown (convert_to_image_type (input_position));
		}

		public bool is_shown(Consts.Ui.Adv_layer_image_type input_image_type){
			return get_image (input_image_type).is_shown ();
		}
	*/
		/*
		public void add_object(GameObject input_object){
			objects_in_layer.Add (input_object);
			input_object.transform.parent = transform;

		}

		public void destroy_immediate_all_objects(){

			foreach(GameObject obj in objects_in_layer){
				DestroyImmediate(obj);
			}

			objects_in_layer = new List<GameObject>();
		}*/
		/*

		public override void run_finished(OBJECTS.Fading_images input_fading_images){

			fading_images.Remove (input_fading_images);

		}

		public override void run_finished(){
			
		}*/
	
	}


}
