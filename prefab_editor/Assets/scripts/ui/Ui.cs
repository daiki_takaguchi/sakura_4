﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.LAYER;

namespace SAKURA{
	[ExecuteInEditMode]
	public class Ui : MonoBehaviour {



		public static Ui ui_static;

		public Layer_list layer_list;

		public Camera world_camera;


		public bool is_debugging_transition=false;


	
		void Start () {

			if(ui_static==null){
				ui_static=this;
			}

		}



		// Update is called once per frame
		void Update () {

		
		}


		public void show(SAKURA.Consts.Ui.Layer_type input_layer_type){
			layer_list.show (input_layer_type);
		}

		public void show_if_hidden(SAKURA.Consts.Ui.Layer_type input_layer_type){
			layer_list.show (input_layer_type);
		}


		public void show(Layer input_layer){

			List<SAKURA.Consts.Ui.Layer_type> layers_conflicting = input_layer.show();

			foreach (SAKURA.Consts.Ui.Layer_type layer in layers_conflicting){
				hide(layer);
			}



		}




		public void hide(SAKURA.Consts.Ui.Layer_type input_layer_type){
			layer_list.hide (input_layer_type);
		}
		/*
		public void hide(UI.Layer input_layer){
			
			input_layer.hide();

			
		}*/

		public void hide_all(){
			layer_list.hide_all ();
		}



	





	}
}
