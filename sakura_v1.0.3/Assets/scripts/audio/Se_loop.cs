﻿using UnityEngine;
using System.Collections;


namespace SAKURA.AUDIO{
	public class Se_loop : MonoBehaviour {

		public AudioSource source;
		//public AudioClip beat;
		//public AudioClip footsteps;

		//SAKURA.Consts.Audio.Se_loop.Se_loop_type

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void play(){

			source.Play ();

		}

		public void change_pan(float input_pan){
			source.panStereo = input_pan;

		}

		public void change_volume(float input_volume){
			source.volume=input_volume;
		
		}
	}
}
