﻿using UnityEngine;
using System.Collections;

namespace SAKURA{
	public class Audio: MonoBehaviour {

		// Use this for initialization

		public static Audio audio_static;

		public AUDIO.Bgm bgm;
		public AUDIO.Se_loop footsteps;
		public AUDIO.Se_loop beat;



		void Start () {
			if(audio_static==null){
				audio_static=this;
			}
		}
		
		// Update is called once per frame
		void Update () {
		
		}

	}
}
