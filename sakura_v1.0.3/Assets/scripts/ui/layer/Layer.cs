﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.UI.LAYER{
	public class Layer : MonoBehaviour {

		// Use this for initialization


		public SAKURA.Ui ui;

		public List<GameObject> objects_in_layer = new List<GameObject>();

		public List<SAKURA.Consts.Ui.Layer_type> layers_conflicting;

		public Variable_list variable_list;



		//public Game_event_list game_event_list;

		void Start () {
		
		}
		
		// Update is called once per frame



		public virtual List<SAKURA.Consts.Ui.Layer_type> show(){

			gameObject.SetActive (true);

			return layers_conflicting;
		}

		public virtual void hide(){

			gameObject.SetActive (false);

			//return layers_conlicting;
		}


		public virtual bool is_shown(){

			return gameObject.activeSelf;
		}
		public void add_object(GameObject input_object){
			objects_in_layer.Add (input_object);
			input_object.transform.parent = transform;

		}

		public void destroy_immediate_all_objects(){

			foreach(GameObject obj in objects_in_layer){
				DestroyImmediate(obj);
			}

			objects_in_layer = new List<GameObject>();
		}
	


	
	}
	
}
