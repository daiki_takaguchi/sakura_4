using UnityEngine;
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.UI.OBJECTS{
	public class Text_line : MonoBehaviour {

		//public UILabel[] labels;
		//public Text parent_text;

		public string input_text = "";
		public string current_text="";
		public bool is_full=false;

		public UIFont font;

		public UIWidget.Pivot pivot;

		public SAKURA.Consts.Ui.Layer_depth_type depth;

		[HideInInspector] public bool japanese_hyphenation=true;
		[HideInInspector] public bool parentheses_indentation=true;

		//public int default_font_size;

		public List<UILabel> labels =new List<UILabel>() ;

		public UILabel current_label=null;
	

		public float max_height;
		public float max_width;

		public float height=0;
		public float prev_labels_width=0;
		public float current_label_width=0;


		private int system_max_labels=100;
		//private int system_max_char=200;

	

		// Use this for initialization
		void Start () {

		
		}
		
		// Update is called once per frame
		void Update () {
			//print (label.relativeSize);


		}




		public Vector2 get_size(){
		
			return new Vector2(prev_labels_width+current_label_width,height);

		}

		public void clear_text(){
			current_text = "";
			is_full = false;
			height = 0;

			prev_labels_width = 0;
			current_label_width = 0;

;


			foreach (UILabel label in labels) {
				DestroyImmediate(label.gameObject);
			}
		}

		public bool Add_text(string temp_input_text,Consts.Ui.Objects.Text_mode input_mode, List<Text_tag> input_text_tags, bool input_is_parentheses_mode,out string added_text, out string remaining_text,out List<Text_tag> output_text_tags, out bool output_is_parentheses_mode){

			input_text = temp_input_text;
			string current_remaining_text = input_text;
			added_text = "";

			bool current_is_parentheses_mode = input_is_parentheses_mode;


			bool is_line_finished = false;

			List<Text_tag> current_text_tags = input_text_tags;

			if(current_label==null){

				create_label (current_text_tags[current_text_tags.Count -1]);
			
			}

			int loop_count = 0;

			bool finish_now = false;

			while(!finish_now&&!is_line_finished&&loop_count<system_max_labels){


				if(current_remaining_text.Length!=0){


					if(parentheses_indentation){
						if(current_text==""){
							if( current_is_parentheses_mode){
								current_label.text+="　";
							}

						}
					}


					int loop_count_2 = 0;

					bool is_parse_finished=false; 

					char current_char=current_remaining_text[0];
					char next_char='\0';
					
					while (!is_parse_finished&&loop_count_2<system_max_labels&&current_remaining_text.Length!=0){

						current_char=current_remaining_text[0];
						next_char='\0';

						if(current_remaining_text.Length>=2){
							next_char=current_remaining_text[1];
						}



						if(current_char=='「'){
							current_is_parentheses_mode=true;
						}else if(current_char=='」'){
							current_is_parentheses_mode=false;

						}else if(current_char=='<'){

							if(next_char=='<'){

								current_remaining_text=current_remaining_text.Substring (1,current_remaining_text.Length-1);
								if(current_remaining_text.Length>=2){
									next_char=current_remaining_text[1];
								}else{
									next_char='\0';
								}
								is_parse_finished=true;

							}else{

								int loop_limit=current_remaining_text.Length;

								for(int i=0;i<loop_limit;i++){
									if(current_remaining_text[i]=='>'){

										//print (current_remaining_text.Substring (1,i-1));

										try_parse_tag(current_remaining_text.Substring (1,i-1),current_text_tags,out current_text_tags);

										current_remaining_text=current_remaining_text.Substring ((i+1),current_remaining_text.Length-(i+1));



										break;
									}

								}
							}

						}else{
							is_parse_finished=true;
						}

						loop_count_2++;
					}

					if(current_remaining_text.Length==0){

						is_line_finished=true;
						is_full=true;

					}else if(try_add_one_char(current_label,current_remaining_text,current_char,next_char,out current_remaining_text)){

						added_text+=current_char.ToString ();

					}else{

						is_line_finished=true;
						is_full=true;
					}
			
					if(input_mode==Consts.Ui.Objects.Text_mode.type_writer){
						finish_now=true;
					}
					

				}

				loop_count++;

			}

			remaining_text = current_remaining_text;

			/*
			if(transform.parent.name=="person_name"){
				print ("input:"+input_text+" label:"+current_label.text+" remaining:"+remaining_text+" "+is_line_finished);
			}*/

			output_text_tags = current_text_tags;
			output_is_parentheses_mode = current_is_parentheses_mode;
			return is_line_finished;
		}

		public void create_label(Text_tag input_text_tag){

			create_label(input_text_tag, new Vector2(0,0));
		}

		public void create_label(Text_tag input_text_tag, Vector2 input_position){

			prev_labels_width += current_label_width;

			GameObject temp= new GameObject();
			
			temp.transform.parent = this.transform;

			temp.name = "text_label_" + labels.Count;
			temp.layer = gameObject.layer;
			temp.transform.localPosition = new Vector3 (input_position.x+(input_text_tag.font_size/2.0f),input_position.y,0);
			temp.transform.localScale = new Vector3 (input_text_tag.font_size,input_text_tag.font_size,1);
			UILabel temp_label = temp.AddComponent<UILabel> ();
			temp_label.pivot = pivot;
			temp_label.text = "";
			temp_label.color = input_text_tag.font_color;
			SAKURA.UI.NGUI.NGUI_depth ngui_depth = temp.AddComponent<SAKURA.UI.NGUI.NGUI_depth> ();
			ngui_depth.layer_depth_type = depth;
			temp_label.font = font;


			if(height<input_text_tag.font_size){ 
				height=input_text_tag.font_size;
			}


			current_label = temp_label;

			labels.Add (current_label);

		}

		public bool try_add_one_char(UILabel input_label,string input_current_remaining_text,char input_char,char next_char, out string output_current_remaining_text){

				bool is_success = false;
				


				output_current_remaining_text = input_current_remaining_text;


		
				string prev_text = new string (input_label.text.ToCharArray ());
				

				input_label.text += input_char.ToString ();
				
				float temp_width = input_label.relativeSize.x*input_label.transform.localScale.x;


				if(temp_width<=max_width){
					

					if(next_char=='\0'){

						is_success=true;
						

					}else if(is_char_japanese_hyphenation_ending(input_char)||is_char_japanese_hyphenation_beginning(next_char)){
						
						input_label.text += next_char.ToString ();
						
						float temp_width_2 = input_label.relativeSize.x*input_label.transform.localScale.x;

						if(temp_width_2<=max_width){
							
							input_label.text = prev_text+input_char.ToString ();
							is_success=true;
							

						}else{
							print (prev_text);
							is_success=false;
						}


					}else{
						is_success=true;
					}


					}
				
			
				

				if(is_success){

					output_current_remaining_text= input_current_remaining_text.Substring (1,input_current_remaining_text.Length-1);

					current_label_width= temp_width;
					current_text+=input_char.ToString ();

				}else{
					
					input_label.text=prev_text;

				}


			

			return is_success;
		}

		public bool is_char_japanese_hyphenation_beginning(char input_char){
			bool output = false;

			if(japanese_hyphenation){
				string illigal_chars = "。、！？!?」";

				foreach (char illigal_char in illigal_chars) {
					if(input_char==illigal_char){
						output=true;
					}
				}
			}

			return output;

		}
		public bool is_char_japanese_hyphenation_ending(char input_char){
			bool output = false;
			if(japanese_hyphenation){
				string illigal_chars = "「";
				
				foreach (char illigal_char in illigal_chars) {
					if(input_char==illigal_char){
						output=true;
					}
				}
			}
			
			return output;
		}

		public string get_inside_quotes(string input_string){

			bool is_double_quote = false;
			bool is_inside_quote = false;
			string output = "";

			foreach (char one_char in input_string) {
				if(!is_inside_quote){

					if(one_char=='"'){
						is_inside_quote=true;
						is_double_quote=true;
					}else if(one_char=='\''){
						is_inside_quote=true;
						is_double_quote=false;
					}else{
					}

				}else{
					if(is_double_quote){

						if(one_char=='"'){
							break;
						}else{
							output+=one_char.ToString();
						}

					}else{
						if(one_char=='\''){
							break;
						}else{
							output+=one_char.ToString();
						}
					}
				}

				
			
			}

			return output;

		}

		public string normalize_spaces(string input_string){

			string output = "";

			bool is_prev_space = false;

			foreach(char one_char in input_string){

				if(one_char==' '||one_char=='　'){

					if(!is_prev_space){
						output+=one_char.ToString ();
					}

					is_prev_space=true;

				}else{

					output+=one_char.ToString ();

					is_prev_space=false;
				}





			}



			return output;
		}



		public void try_parse_tag(string input_text,List<Text_tag> input_text_tags, out List<Text_tag> output_text_tags){



			string current_parsed_text = normalize_spaces (input_text);

			string[]  current_parsed_text_array= current_parsed_text.Split (new char [] {' ', '　'});


			List<Text_tag> current_text_tags = input_text_tags;

			float new_font_size = input_text_tags [input_text_tags.Count - 1].font_size;
			Color new_color = input_text_tags [input_text_tags.Count - 1].font_color;
			SAKURA.Consts.Ui.Objects.Text_vertical_align new_text_vertical_align = input_text_tags [input_text_tags.Count - 1].text_vertical_align;

			bool is_error = false;
			bool is_close_tag = false;

			foreach (string one_text in current_parsed_text_array){

				//print (one_text);

				string[]  one_text_array= one_text.Split (new char [] {'=', '＝'});

				if(one_text_array.Length==2){

					if(one_text_array[0].Trim()=="size"){

						string temp_string=get_inside_quotes(one_text_array[1].Trim());
						float temp_font_size=new_font_size;

						if(float.TryParse(temp_string.Trim(),out temp_font_size )){

							new_font_size=temp_font_size;
						}else{
							print (temp_string.Trim());

							is_error=true;
							Debug.LogError ("text tag parse error!");
						};

					}else if(one_text_array[0].Trim()=="color"){



						int color_r=0;
						int color_g=0;
						int color_b=0;
						int color_a=255;

						string temp_string=get_inside_quotes(one_text_array[1].Trim ());

						//print (temp_string);

						if(temp_string[0]=='#'){


							if(temp_string.Length==7||temp_string.Length==9){

	

								color_r=decode_color_code(temp_string.Substring(1,2));
								color_g=decode_color_code(temp_string.Substring(3,2));
								color_b=decode_color_code(temp_string.Substring(5,2));


								if(temp_string.Length==9){
									color_a=decode_color_code(temp_string.Substring(7,2));
								}

							
							}else {
								is_error=true;
								Debug.LogError ("color code parse error!");
							}





						}else{

						}

						new_color= new Color(color_r,color_g,color_b,color_a);

					}else if(one_text_array[0].Trim()=="vertical-align"){

					}
						
				}else if(one_text_array.Length==1){

					if(one_text_array[0].Trim()=="font"){
					
					}else if(one_text_array[0].Trim()=="/font"){

						current_text_tags.RemoveAt(current_text_tags.Count-1);
						text_tag_changed(current_text_tags[current_text_tags.Count-1]);
						is_close_tag=true;

					}else{
						print (one_text_array[0].Trim());
						is_error=true;
						Debug.LogError ("text tag parse error!");
					}

				}else{
				
				}
			}

			if(!is_error&&!is_close_tag){
				Text_tag new_text_tag = new Text_tag (new_font_size, new_color,new_text_vertical_align);
				current_text_tags.Add (new_text_tag);
				text_tag_changed(new_text_tag);
			}

			output_text_tags = current_text_tags;

		}



		public int decode_color_code(string input_string){

			int output = 0;

			CultureInfo culture=new CultureInfo("en-US");

			if (int.TryParse (input_string, System.Globalization.NumberStyles.HexNumber, culture, out output)) {
			
			}else{
				Debug.LogError ("color code parse error!");
			};

			return output;
		}

		public void text_tag_changed(Text_tag input_text_tag){

			create_label(input_text_tag, new Vector2(prev_labels_width+ current_label_width,0));
		}


		/*
		public UILabel[] push_label(UILabel[] input_labels, UILabel pushed_label){

		}
		*/


	}

}
