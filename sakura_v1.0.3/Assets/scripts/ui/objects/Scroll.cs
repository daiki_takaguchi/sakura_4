﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.UI.OBJECTS{
	public class Scroll : MonoBehaviour {

		public Scroll_collider scroll_collider;
		public Scroll_internal_panel scroll_internal_panel;

		public Vector3 screen_limit_max ;
		private List<float> screen_limit_max_list;

		public Vector3 screen_limit_min;
		private List<float> screen_limit_min_list;

		//public Vector3 internal_panel_size;
//		/public Transform internal_panel_transform;


		public List<GameObject> children= new List<GameObject>();

		public bool is_draggable=false;
		public bool is_pressed=false;
		public bool is_moving=false;

		public Vector2 press_started_position;
		public Vector3 press_started_transform_position;

		public float adjust_spring_power=300.0f;

		public Vector3 inertia_abs=new Vector3(0,0,0);
		public Vector3 inertia_sign=new Vector3(0,0,0);
		public float inertia_brake_power= 2000.0f;


		// Use this for initialization
		void Start () {

			screen_limit_max_list = Sakura.vector3_to_floats (screen_limit_max);
			screen_limit_min_list = Sakura.vector3_to_floats (screen_limit_min);
			//limit_y_bottom = -(Screen.height/ 2) * (640.0f / Screen.width) + 200.0f;
		}
		
		// Update is called once per frame
		void Update () {

			if(is_draggable){

				if(is_pressed){

					Vector2 pressed_position= new Vector2(0,0);

					if(Sakura.try_get_touched_position(out pressed_position)){

						Vector3 prev_position= scroll_internal_panel.transform.localPosition;

						Vector3 difference= (pressed_position-press_started_position);

						Vector3 next_position= press_started_transform_position+ new Vector3(0,difference.y,0);

						scroll_internal_panel.transform.localPosition=next_position;

						divide_vector_into_abs_and_sign((next_position-prev_position)/Time.deltaTime,out inertia_abs, out inertia_sign);


						
					}else{
						Debug.LogError("cannot detect pressed position!");
					}


				}else{


					if(is_moving){
					
						//bool is_spring_on=true;
						//bool is_inertia_on=true;

						Vector3 out_bound_abs;
						Vector3 out_bound_sign;

						get_out_bound_pair(scroll_internal_panel.transform.localPosition, out out_bound_abs, out out_bound_sign);

						out_bound_abs+= new Vector3(1,1,1);


						//print (out_bound_abs);
						//Vector3 spring_position= new Vector3(0,0,0);
						Vector3 spring_position= -Sakura.mfe(out_bound_abs,out_bound_sign)*adjust_spring_power*Time.deltaTime;

						//List<float> out_bound_abs_list= Sakura.Vector_to_floats(out_bound_abs);
						List<float> out_bound_sign_list= Sakura.vector3_to_floats(out_bound_sign);


						Vector3 inertia_position= new Vector3(0,0,0);


						inertia_position= Sakura.mfe(inertia_sign,inertia_abs)*Time.deltaTime;

						inertia_abs-=inertia_abs*Time.deltaTime*inertia_brake_power;
							
						inertia_abs=high_pass_filter(inertia_abs,1);

						List<float> inertia_abs_list= Sakura.vector3_to_floats(inertia_abs);
		
							


						Vector3 temp_next_position= scroll_internal_panel.transform.localPosition+spring_position+inertia_position;

						scroll_internal_panel.transform.localPosition=temp_next_position;

						bool temp=false;

						for(int i=0;i<3;i++){
							if(inertia_abs_list[i]!=0||out_bound_sign_list[i]!=0){
								temp=true;
							}

						}

						is_moving=temp;

						//List<float> temp_next_position_list=Sakura.Vector_to_floats(temp_next_position);

						//Vector3 next_out_bound_abs;

						//Vector3 next_out_bound_sign;

						//get_out_bound_pair(temp_next_position, out next_out_bound_abs, out next_out_bound_sign);

						//next_out_bound_abs= high_pass_filter (next_out_bound_abs,1);

						//List<float> next_out_bound_abs_list= Sakura.Vector_to_floats(next_out_bound_abs);
						//List<float> next_out_bound_sign_list= Sakura.Vector_to_floats(next_out_bound_sign);

						//List<float> next_position_list= new List<float>(3);

						/*

						for (int i =0;i<3;i++){

							if(next_out_bound_abs_list[i]==0){

								if(position_type==-1){

									Vector3 temp =transform.localPosition;

									transform.localPosition=new Vector3(temp.x,0,temp.z);

									inertia_abs[i]=0;



								}else if(position_type==1){
				

									Vector3 temp =transform.localPosition;

					

									if(total_cells_height-available_screen_length<0){
										transform.localPosition=new Vector3(temp.x,0,temp.z);
									}else{
										transform.localPosition=new Vector3(temp.x,total_cells_height-available_screen_length,temp.z);
									}

									inertia_abs[i]=0;



								}else{
									next_position_list[i]=temp_next_position_list[i];
								}

							}else{

								

								next_position_list[i]=temp_next_position_list[i];

								

							
							}

						}*/

					
					}
					



				}

			}

		}



		public void divide_vector_into_abs_and_sign(Vector3 input_vector, out Vector3 output_abs, out Vector3 output_sign){

			output_abs = new Vector3 (Mathf.Abs(input_vector.x),Mathf.Abs(input_vector.y),Mathf.Abs(input_vector.z));
			
			output_sign = get_sign_or_zero (input_vector);
		}

		public void get_out_bound_pair(Vector3 input_posotion, out Vector3 output_abs, out Vector3 output_sign){

		

			divide_vector_into_abs_and_sign (get_out_bound_position(input_posotion), out output_abs, out output_sign);

		}

		public Vector3 get_out_bound_position(Vector3 input_posotion){

			//Vector3 output =  new Vector3(0,0,0);
			//float output_x = 0;
			//float output_y = 0;
			//float output_z = 0;

			List<float> output = new List<float> ();
			output.Add (0);
			output.Add (0);
			output.Add (0);

			List<float> input_position_list = Sakura.vector3_to_floats (input_posotion);
		

			for(int i=0;i<3;i++){

		

				if(input_position_list[i]>screen_limit_max_list[i]){
					output[i]=input_position_list[i]-screen_limit_max_list[i];
				}else if(input_position_list[i]<screen_limit_min_list[i]){
					output[i]= input_position_list[i]-screen_limit_min_list[i];
				}
				
			}
			return Sakura.floats_to_vector3(output);
		}

		public Vector3 get_sign_or_zero(Vector3 input_vector){
			

			return  new Vector3 (get_sign_or_zero(input_vector.x),get_sign_or_zero(input_vector.y),get_sign_or_zero(input_vector.z));
			
		}

		public float high_pass_filter(float input_float,float input_threshold){

			float output = 0.0f;

			if(input_float>input_threshold){
				output=input_float;
			}

			return output;
		}

		public Vector3 high_pass_filter(Vector3 input_vector,float input_threshold){

			
			return  new Vector3(high_pass_filter(input_vector.x,input_threshold),high_pass_filter(input_vector.y,input_threshold),high_pass_filter(input_vector.z,input_threshold));
		}

		public float get_sign_or_zero(float input_float){

			float output = 0;


			if(input_float>0){
				output=1.0f;
			}else if(input_float<0){
				output=-1.0f;
			}

			return output;

		}




		public void pressed(bool input_is_pressed){


			is_pressed = input_is_pressed;

			if (input_is_pressed) {

				press_started_transform_position =scroll_internal_panel.transform.localPosition;

				if (!Sakura.try_get_touched_position (out press_started_position)) {
					Debug.LogError ("cannot detect pressed position!");
				}
			} else {
				is_moving=true;
			}

		}

		public void drag(Vector2 delta){
			scroll_internal_panel.transform.localPosition += new Vector3 (0,delta.y,0);
		}

		public void initialize(){
			initialize_position();
		}

		public void initialize_position(){
			Vector3 temp =scroll_internal_panel.transform.localPosition;
			
			scroll_internal_panel.transform.localPosition=new Vector3(temp.x,screen_limit_max.y,temp.z);
		}




	}
}
