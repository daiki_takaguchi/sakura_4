﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.NGUI;

namespace SAKURA.UI.OBJECTS{
	public class Scroll_internal_panel_backlog : Scroll_internal_panel {

		//public float offset_y;
		//public Vector3 size;

		// Use this for initialization

		public Text text;

		void Start () {
			//limit_y_bottom = -(Screen.height/ 2) * (640.0f / Screen.width) + 200.0f;
		}
		
		// Update is called once per frame
		void Update () {

		}

		public override Vector3 get_size(){

			float x = text.max_width;
			float y = text.current_height;

			return new Vector3(x,y,0);

		}

	}
}
