﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.UI.OBJECTS{
	public class Text_tag {

		public float font_size;
		public Color font_color;
		public SAKURA.Consts.Ui.Objects.Text_vertical_align text_vertical_align ;


		public Text_tag(float input_font_size,Color input_font_color,SAKURA.Consts.Ui.Objects.Text_vertical_align input_text_vertical_align ){
			font_size = input_font_size;
			font_color = input_font_color;
			text_vertical_align = input_text_vertical_align;
		}


	}

}
