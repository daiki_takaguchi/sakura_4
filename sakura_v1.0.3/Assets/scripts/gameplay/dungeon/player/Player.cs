﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.GAMEPLAY.DUNGEON{
	public class Player : MonoBehaviour {

	// Use this for initialization

		public Player_object player_object;
		public Camera player_camera;

		public Variable_object_position variable_position;
		public Variable_object_rotation variaboe_rotation;

		public List<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point> current_points= new List<SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point>();


		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public Vector3 get_local_position(){
			return new Vector3 (0,0,0);
		}

		public Vector3 get_global_position(){
			return new Vector3 (0, 0, 0);
		}

		public Quaternion get_local_rotation(){
			return Quaternion.identity;
		}

		
		public SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point get_current_point(){
			
			SAKURA.DATA.DUNGEON_SETTINGS.Enemy_move_point output = null;
			
			if(current_points.Count>0){
				output=current_points[0];
			}
			
			return output;
		}


	}
}