﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using SAKURA.DATA.SCENE_SETTINGS;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;


namespace SAKURA{
	public class Gameplay : MonoBehaviour {

		// Use this for initialization

		public static Gameplay gameplay_static;
		
		public SAKURA.Ui ui;

		public GAMEPLAY.Dungeon dungeon;
		public GAMEPLAY.Adv adv;

		public SAKURA.Consts.Gameplay.Game_state_type prev_game_state;
		public SAKURA.Consts.Gameplay.Game_state_type current_game_state;

		//public Chapter_settings current_chapter_settings;
		public GameObject current_scene_gameobject;
		public Scene_settings current_scene_settings;

		public List<Variable_list> all_variable_lists;

		public Variable_list variable_list_gameplay;
		public Variable variable_touch_panel;

		//public List<Action> touch_waiting_actions= new List<Action>();

		public Animator game_state_transition;

		void Start () {

			if(gameplay_static==null){
				gameplay_static=this;
			}

			//dungeon.gameObject.SetActive (false);
			//adv.gameObject.SetActive (false);

			ui.hide_all ();
			change_game_state (current_game_state);
		}
		
		// Update is called once per frame
		void Update () {
			if (!Application.isPlaying) {
				if(gameplay_static==null){
					gameplay_static=this;
				}
			}

		}

		public void run_current_scene(){
			if(current_scene_settings!=null){
				current_scene_settings.run ();
			}
		}

		public void change_game_state (SAKURA.Consts.Gameplay.Game_state_type next_game_state){
			change_game_state (next_game_state,null);
		}


		public void change_game_state(SAKURA.Consts.Gameplay.Game_state_type next_game_state,Jump jump){


			if(next_game_state==Consts.Gameplay.Game_state_type.title){
				ui.show (Consts.Ui.Layer_type.title);
			}else if(next_game_state==Consts.Gameplay.Game_state_type.adv){

				ui.show (Consts.Ui.Layer_type.adv);
				adv.gameObject.SetActive (true);

			}else if(next_game_state==Consts.Gameplay.Game_state_type.dungeon_test){

				ui.show (Consts.Ui.Layer_type.dungeon);
				dungeon.change_dungeon_state (Consts.Gameplay.Dungeon.Dangeon_state_type.enemy_test);

			}else if(next_game_state==Consts.Gameplay.Game_state_type.backlog){
				ui.show (Consts.Ui.Layer_type.backlog);

			}

			prev_game_state = current_game_state;
			current_game_state = next_game_state;
			
		}

		public void game_state_changed(SAKURA.Consts.Gameplay.Game_state_type input_game_state){
			
		}

		/*
		public Variable get_variable(string variable_name){

		}*/

		public bool load_scene(GameObject input_game_object){

			bool output = false;

			//print ("loading");

			Scene_settings temp= input_game_object.GetComponent<Scene_settings> ();

			if(temp!=null){



				DestroyImmediate(current_scene_gameobject);

				current_scene_settings=temp;

				current_scene_gameobject= (GameObject) Instantiate(input_game_object);
				current_scene_gameobject.transform.parent=transform;
				current_scene_settings=current_scene_gameobject.GetComponent<Scene_settings>();

				run_current_scene ();
				
				output=true;

			}

		

			return output;
				
		}

		public Variable get_variable(System.Type input_variable_type){
			
			Variable output = null;
			
			if(input_variable_type==typeof(Variable_touch_panel)){
				output=variable_touch_panel;
			}
			
			return output;
		}





	}
}
