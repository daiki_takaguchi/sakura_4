﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_settings : Adv_settings_base {

		// Use this for initialization



		public string scene_name;
		public int chapter_no;
	


		public List<Scene_sequence> scene_sequences;

		public List<Scene_settings> prev_scene_settings;
		public List<Scene_settings> next_scene_settings;

		void Start () {
		
		}
		
		// Update is called once per frame
		/*
		void Update () {
		
			if(!Application.isPlaying){



				if(add_action_now){

					add_action_now=false;


					if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.change_person_image){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Change_person_image>();

					}else if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.wait){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Wait>();
					}else if(action_type_to_add==Consts.Data.Scene_settings.Action.Action_type.adv_text_action){
						GameObject temp= new GameObject();
						temp.transform.parent=transform;
						temp.AddComponent<SCENE_SETTINGS.Adv_text_action>();
					}
				
				}

				/*
				if(prev_actions_count==actions.Count){




					foreach (SCENE_SETTINGS.Action action in actions){
						
						if(action!=null){
							action.name="";
							//count++;
							
							//new_actions.Add (action);
						}
					}

					int count=0;

					List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();

		
					foreach (SCENE_SETTINGS.Action action in actions){

						if(action!=null){
							action.name+="Action:"+count+" "+ action.action_type.ToString()+" ";
							count++;

							new_actions.Add (action);
						}
					}

					actions=new_actions;
						
						
				}else{

					//print ("!");
					List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();

					int count=0;
					foreach (SCENE_SETTINGS.Action action in actions){


						new_actions.Add (action);

						count++;

					}

					actions=new_actions;
					prev_actions_count=actions.Count;

				}


			}

				List<SCENE_SETTINGS.Action> new_actions=new List<SAKURA.DATA.SCENE_SETTINGS.Action>();
				int count=0;

				foreach(Transform child in transform){

					SCENE_SETTINGS.Action action=child.GetComponent<SCENE_SETTINGS.Action>();

					if(action!=null){
						action.name="Action:"+count+" "+ action.action_type.ToString()+" ";
						count++;
						
						new_actions.Add (action);
					}

				}
				
				actions=new_actions;
			}

			
		}
		*/

		public void run(){
			foreach(Scene_sequence scene_sequence in scene_sequences){
				scene_sequence.run ();
			}
		}
		


	}
}
