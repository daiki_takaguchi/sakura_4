﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_string : Variable {


		public string initial_value;
		public string current_value;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame


		public override void initialize(){
			current_value = initial_value;
		}

	}
}
