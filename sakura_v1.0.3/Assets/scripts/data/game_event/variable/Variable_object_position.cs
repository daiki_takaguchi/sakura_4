﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_object_position : Variable {

		//public static Variable_backlog variable_backlog_static;
		public Vector3 inital_position;
		public SAKURA.Consts.Data.Game_event.Variable.Variable_object_position_initial_type inital_position_type;
		public SAKURA.Consts.Data.Game_event.Variable.Variable_object_position_state state;
		public float animation_start_time;
		public float animation_end_time_from_start;
		public Transform object_position_transform;

		public Vector3 animation_local_position_from;

		public Vector3 animation_local_position_to;


		//private SAKURA.Consts.Data.Scene_settings.Action.Person_name prev_person;

		// Use this for initialization
		void Start () {
		
		}

		void Update(){
			if (state == Consts.Data.Game_event.Variable.Variable_object_position_state.animation_on) {
			

				run_animation();
			}	
		}

		public void run_animation(){

			bool is_animation_end = false;

			float time_from_start = (Time.time - animation_start_time);

			if(time_from_start<animation_end_time_from_start){

				float ratio = time_from_start / animation_end_time_from_start;

				object_position_transform.localPosition =Vector3.Lerp(animation_local_position_from,animation_local_position_to,ratio);

			}else{
				is_animation_end=true;
			}

			if(is_animation_end){



				object_position_transform.localPosition =animation_local_position_to;

				change_state(Consts.Data.Game_event.Variable.Variable_object_position_state.none);
			}
		}

		public void change_state(SAKURA.Consts.Data.Game_event.Variable.Variable_object_position_state input_state){
			
			state = input_state;

			waiting_game_event_list.initalize_action_pointer ();
			waiting_game_event_list.run_events ();
			
		}

		public void change_object_local_position_animation(Vector3 input_position, float input_time){



			if(state==Consts.Data.Game_event.Variable.Variable_object_position_state.none){


				animation_local_position_from=get_object_local_position();
				animation_local_position_to=input_position;
		
				animation_start_time=Time.time;
				animation_end_time_from_start=input_time;

				change_state(Consts.Data.Game_event.Variable.Variable_object_position_state.animation_on);

			}
		}


		public void add_object_local_position(Vector3 input_position){
			if(state==Consts.Data.Game_event.Variable.Variable_object_position_state.none){
				object_position_transform.localPosition += input_position;
			}
		}


		public void change_object_local_position(Vector3 input_position){
			if(state==Consts.Data.Game_event.Variable.Variable_object_position_state.none){
				object_position_transform.localPosition = input_position;
			}
		}

		public Vector3 get_object_local_position(){

			return object_position_transform.localPosition;

		}

		public Vector3 get_object_global_position(){
			
			return object_position_transform.position;
			
		}



	
	}
}
