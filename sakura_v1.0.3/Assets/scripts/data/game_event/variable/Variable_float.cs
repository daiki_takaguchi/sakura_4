﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_float : Variable {


		public float initial_value;
		public float current_value;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame

		public override void initialize(){
			current_value = initial_value;
		}

	}
}
