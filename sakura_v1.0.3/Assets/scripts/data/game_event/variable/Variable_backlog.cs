﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_backlog : Variable {

		public static Variable_backlog variable_backlog_static;

		public string initial_value;
		public string current_value;

		private SAKURA.Consts.Data.Scene_settings.Action.Person_name prev_person;

		// Use this for initialization
		void Start () {
			if(variable_backlog_static==null){
				variable_backlog_static=this;
			}
		}
		
		// Update is called once per frame


		public override void initialize(){
			current_value = initial_value;
		}

		public void add_text(){

		}

	}
}
