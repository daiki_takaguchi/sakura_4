﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.VARIABLE;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Move_object : Action {

		Variable_object_position variable_object_position;
		Variable_object_rotation variable_object_rotation;

		public SAKURA.Consts.Data.Scene_settings.Action.Move_object_operation operation;

		public Vector3 target_position;

		public Quaternion target_rotation;

		public float animation_end_relative_time;

		[SerializeField] private bool animation_loop=true;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void initalize(Variable_object_position input_position ,Variable_object_rotation input_rotation){
			variable_object_position= input_position;
			variable_object_rotation = input_rotation;
		}

		public override void run(Action input_action){
			
			parent_action = input_action;
			
			run_internal ();
		}
		
		public override void run(Action_list input_action_list){
			
			parent_action_list = input_action_list;
			
			run_internal ();
			
		}

		private void run_internal(){
			//print (operation);
			if (operation == Consts.Data.Scene_settings.Action.Move_object_operation.rotate_left_animation) {

				target_rotation=accurate_left_rotation(variable_object_rotation.get_object_local_rotation());

				variable_object_rotation.change_object_local_rotation_animation(target_rotation,animation_end_relative_time);

			}else if(operation==Consts.Data.Scene_settings.Action.Move_object_operation.rotate_right_animation){


				target_rotation=accurate_right_rotation(variable_object_rotation.get_object_local_rotation());

				variable_object_rotation.change_object_local_rotation_animation(target_rotation,animation_end_relative_time);

			

			}else if(operation==Consts.Data.Scene_settings.Action.Move_object_operation.check_collision_and_move_forward_animation){

				Quaternion rotation =variable_object_rotation.get_object_local_rotation();
				
				float grid_length=Data.data_static.game_settings.player_step_length;
				//float step_length=Data.data_static.game_settings.player_step_length;
				
				Vector3 grid=new Vector3( grid_length, grid_length, grid_length);

				Vector3 to= rotation* new Vector3(0,0,grid_length);

				target_position=Sakura.closest_grid_point( variable_object_position.get_object_local_position()+
				                                       to,grid);



				if(is_ok_to_move(to)){

					variable_object_position.change_object_local_position_animation(target_position,animation_end_relative_time);
				}

			}else if(operation==Consts.Data.Scene_settings.Action.Move_object_operation.check_collision_and_move_forward_animation_loop){


				Quaternion rotation =variable_object_rotation.get_object_local_rotation();
				
				float grid_length=Data.data_static.game_settings.player_step_length;
				//float step_length=Data.data_static.game_settings.player_step_length;

				
				Vector3 grid=new Vector3( grid_length, grid_length, grid_length);

				Vector3 to= rotation* new Vector3(0,0,grid_length);
				
				target_position=Sakura.closest_grid_point( variable_object_position.get_object_local_position()+
				                                         to,grid);

				if(is_ok_to_move(to)){

					animation_loop=true;

					variable_object_position.change_object_local_position_animation(target_position,animation_end_relative_time);

					notify_me_in_condition();
				}



			}

		}

		public Quaternion accurate_left_rotation(Quaternion input_quaternion){

			Vector3 current_direction=Sakura.closest_six_axis(input_quaternion* Vector3.forward);

			//print (current_direction);

			Quaternion output = Quaternion.identity;

			if(current_direction==Vector3.forward){

				output = Quaternion.Euler(new Vector3(0,270,0));
			
			}else if(current_direction==Vector3.left){
				output = Quaternion.Euler(new Vector3(0,180,0));
			}else if(current_direction==Vector3.back){
				output = Quaternion.Euler(new Vector3(0,90,0));
			}else if(current_direction==Vector3.right){
				output = Quaternion.Euler(new Vector3(0,0,0));
			}else{
				Debug.LogError("something wrong!");
			}

			return output;
		}

		public Quaternion accurate_right_rotation(Quaternion input_quaternion){
			
			Vector3 current_direction=Sakura.closest_six_axis(input_quaternion* Vector3.forward);
			
			print (current_direction);
			
			Quaternion output = Quaternion.identity;
			
			if(current_direction==Vector3.forward){
				
				output = Quaternion.Euler(new Vector3(0,90,0));
				
			}else if(current_direction==Vector3.left){
				output = Quaternion.Euler(new Vector3(0,0,0));
			}else if(current_direction==Vector3.back){
				output = Quaternion.Euler(new Vector3(0,270,0));
			}else if(current_direction==Vector3.right){
				output = Quaternion.Euler(new Vector3(0,180,0));
			}else{
				Debug.LogError("something wrong!");
			}
			
			return output;
		}
		/*
		public Quaternion right_from_rotation(Quaternion input_quaternion){
			
			Vector3 temp=
				
				
		}*/


		private void notify_me_in_condition(){
		
			if(created_game_event_list==null){
				created_game_event_list=gameObject.AddComponent<Game_event_list>();
			}

			Game_event game_event =created_game_event_list.create_game_event();
			
			Condition_object_position condition =(Condition_object_position) game_event.add_condition<Condition_object_position>();

			condition.variable_object_position = variable_object_position;

			condition.condition_state = Consts.Data.Game_event.Variable.Variable_object_position_state.none;

			Notify_variable_changed notify =(Notify_variable_changed) game_event.add_action<Notify_variable_changed>();
			
			notify.initialize(this,variable_object_position);
			
			game_event.add_event_to_variables();
	
			
			
		}


		public bool is_ok_to_move(Vector3 to){

			Vector3 from = variable_object_position.get_object_global_position ();


			bool output = false;

			RaycastHit hit;

			float step_length = Data.data_static.game_settings.player_step_length;

			if (Physics.Raycast (from, to, out hit)) {

				float distance = hit.distance;
				//print (distance);
				if(distance>step_length){
					output=true;
				}

			}

			return output;
		}

		public override void variable_changed(Variable input_variable,Game_event input_game_event){
			if(input_variable is Variable_object_position){
				if(operation==Consts.Data.Scene_settings.Action.Move_object_operation.check_collision_and_move_forward_animation_loop){

					Variable_object_position temp= (Variable_object_position) input_variable;


					if(temp.state==Consts.Data.Game_event.Variable.Variable_object_position_state.none){

						created_game_event_list.destroy_all_game_events();

						print ("loop");

						if(animation_loop){
							run_internal();
						}
					}
				}
			

			}else if(input_variable is Variable_touch_panel){



				Variable_touch_panel temp= (Variable_touch_panel) input_variable;

				//print(temp.state.ToString()+animation_loop);

				if(temp.state!=Consts.Ui.Objects.Touch_panel_state.one_finger_long_press){
					animation_loop=false;
				}
			}
		}
	}
}
