﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Change_layer_state :  Action {

		/*
		public override SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.change_layer_state;}
		}*/

		public SAKURA.Consts.Ui.Layer_type layer;

		public Consts.Data.Scene_settings.Action.layer_operation operation;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}



		public override void run(Action_list input_acion_list){


			if(operation==Consts.Data.Scene_settings.Action.layer_operation.show){
				SAKURA.Ui.ui_static.show (layer);
			}else if(operation==Consts.Data.Scene_settings.Action.layer_operation.hide){
				SAKURA.Ui.ui_static.hide (layer);
			}
			
			
			
			input_acion_list.run_next ();
			
		}


	}
}
