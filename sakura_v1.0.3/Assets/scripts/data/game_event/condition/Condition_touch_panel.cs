﻿using UnityEngine;
using System.Collections;
using SAKURA;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.CONDITION{
	public class Condition_touch_panel : Condition {

		/*
		public virtual Consts.Data.Game_event.Condition.Coidition_type condition_type {
			get {
				return Consts.Data.Game_event.Condition.Coidition_type.default_condition;
			}
				
		}*/
		[HideInInspector] bool is_initalized=false;
		[HideInInspector] public Variable_touch_panel variable_touch_panel;

		public Consts.Ui.Objects.Touch_panel_state condition_touch_panel_state;

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () {

		}

		public override void refresh_condition(Condition_list input_condition_list){
			set_variable_touch_panel ();
		}

		public void initialize(){
			//variable_touch_panel = (Variable_touch_panel) GameObject.FindObjectOfType<Gameplay> ().get_variable (typeof(Variable_touch_panel));
			set_variable_touch_panel ();
		}

		public void set_variable_touch_panel(){
			is_initalized = true;
			variable_touch_panel = (Variable_touch_panel) GameObject.FindObjectOfType<Gameplay> ().get_variable (typeof(Variable_touch_panel));
		}

		public override Variable add_event_to_variable(Game_event input_game_event){

			variable_touch_panel.add_game_event (input_game_event);

			return variable_touch_panel;
		}

		public override bool check_condition(){

			if(!is_initalized){
				initialize();
			}

			return variable_touch_panel.state==condition_touch_panel_state;
		}
	}
}
