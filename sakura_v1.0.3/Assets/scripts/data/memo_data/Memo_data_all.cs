﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.DATA.MEMO_DATA {
	[ExecuteInEditMode]
	public class Memo_data_all : MonoBehaviour {

		public Memo_data_list infomation;
		public Memo_data_list explanation;


		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if (!Application.isPlaying) {

				infomation.memo_data_list=  Sakura.get_monos_from_children<Memo_data>(infomation.transform);

				foreach(Memo_data memo_data  in infomation.memo_data_list ){
				
					memo_data.memo_data_type=SAKURA.Consts.Data.Memo_data.Memo_data_type.information;
					memo_data.gameObject.name=memo_data.memo_name;

				}


				explanation.memo_data_list=  Sakura.get_monos_from_children<Memo_data>(explanation.transform);

				foreach(Memo_data memo_data  in explanation.memo_data_list ){

					memo_data.memo_data_type=SAKURA.Consts.Data.Memo_data.Memo_data_type.explanation;
					memo_data.gameObject.name=memo_data.memo_name;

				}
			}
		}
	}
}
