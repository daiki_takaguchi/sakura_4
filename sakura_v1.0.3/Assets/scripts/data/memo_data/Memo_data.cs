using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;

namespace SAKURA.DATA.MEMO_DATA {
	public class Memo_data : MonoBehaviour {

		public  SAKURA.Consts.Data.Memo_data.Memo_data_type memo_data_type;

		public string memo_name;
		public string text;

		public bool is_locked=true;

		public int chapter_no;
		public string scene_name;
		public int sequence_no;
		public int state_no;

		public Unlock_memo unlock_memo;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void unlock(){

			is_locked = false;

		}
	}
}
