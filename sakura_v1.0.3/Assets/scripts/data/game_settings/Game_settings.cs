﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UNITY_EDITOR;

namespace SAKURA.DATA {
	public class Game_settings : MonoBehaviour {

		public bool is_debug;
		public List<float> text_speed;
		public float tap_detection_seconds;
		public float long_press_detection_seconds;
		public float swipe_detection_seconds;
		public float swipe_detection_pixel;

		public Vector3 grid_length = new Vector3 (1.28f,1.28f,1.28f);
		public float player_step_length ;
		public float player_step_finish_time_from_start;
		public float player_rotation_finish_time_from_start;

		public Json_to_action_conversion_list json_to_action_conversion_list;

		//public static float grid_length;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}
	}
}
