﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.RESOURCES{
	public class People_data : MonoBehaviour {

		public Person_data a;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public Texture get_person_image(
			SAKURA.Consts.Data.Scene_settings.Action.Person_name input_name,
			SAKURA.Consts.Data.Scene_settings.Action.Person_image_expression input_expression
			){


			Texture output = null;
			Person_data temp =get_person_data(input_name);
			if(temp!=null){
				output=temp.get_person_image (input_expression);
			}

			return output;
		}

		public string get_person_name(SAKURA.Consts.Data.Scene_settings.Action.Person_name input_name){

			string output = "";

			Person_data temp =get_person_data(input_name);
			if(temp!=null){
				output=temp.get_person_name ();
			}
			
			return output;
		}

		private Person_data get_person_data(SAKURA.Consts.Data.Scene_settings.Action.Person_name input_name){
			Person_data output = null;

			if(input_name==Consts.Data.Scene_settings.Action.Person_name.a){
				output=a;
			}

			return output;
		}
	}
}
