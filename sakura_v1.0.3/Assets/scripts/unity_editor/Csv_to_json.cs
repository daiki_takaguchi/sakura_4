﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;

namespace SAKURA.UNITY_EDITOR{

	[ExecuteInEditMode]

	public class Csv_to_json : MonoBehaviour {

		public bool convert_file;

		public string input_file_name;
		public string output_file_name;


		void Update () {
			if(!Application.isPlaying){
				
				if(convert_file){

					run ();
				
					convert_file=false;
					
				}
				
			}
			
		}

		public void run(){

			List<List<string>> loaded_data = Csv_functions.load (input_file_name);

			JSONObject output = convert_data (loaded_data);


			FileStream fs = new FileStream(output_file_name,FileMode.Create, FileAccess.Write);
			
			StreamWriter sw = new StreamWriter (fs);

			sw.WriteLine(output.ToString());    
			
			sw.Flush();
			sw.Close(); 
			fs.Close ();

		
		}

		public JSONObject convert_data(List<List<string>> input_string){

			JSONObject output = new JSONObject(JSONObject.Type.OBJECT);


			foreach(List<string> row in input_string){

				string temp="";


				foreach (string column in row){
					temp+=column+",";
				}
			

				string action_name= command_to_action(row);

				JSONObject j=row_to_json(row,action_name);

				output.AddField(action_name,j);
			}

			return output;

		}
	



		public string command_to_action(List<string> input_row){

			string output = "";

		
			if(get_column_data(input_row,"command")=="Wait"){

				output="Wait";

			}else if(get_column_data(input_row,"command")=="SE"){

				output="Audio_SE";
					
			}else if(get_column_data(input_row,"command")=="BGM"){

				output="Audio_BGM";

			}else if(get_column_data(input_row,"command")=="Anbience"){

				output="Audio_SE";

			}else if(get_column_data(input_row,"command")=="BG"){

				output="Action_image";

			}else if(get_column_data(input_row,"command")=="Scene"){
				
				output="Jump";

			}else if(get_column_data(input_row,"command")=="Title"){
				
				output="Jump";

			}else if(get_column_data(input_row,"command")=="Select"){
				
				output="Select";


			}else if(get_column_data(input_row,"command")=="Beat"){

				output="Effect";

			}else if(get_column_data(input_row,"command")==""){

				if(get_column_data(input_row,"name")==""){

					output="Action_adv_text";

				}else{

					output="Action_person_image";
				}



			}

			return output;
		
		
		}
		

		public string convert_column_data(List<string> input_row,string input_action_name,string input_column_name,string input_variable_name){

			string output = "";
				
			string temp= get_column_data_exception( input_row,input_action_name,input_column_name, input_variable_name);
			
			if(temp==""){

				output=get_column_data( input_row,input_column_name);

			}else{
				output=temp;
			}

			return output;
		}

		


		public string get_column_data(List<string> input_row,string input_column_name){

			List<string> column_names = new List<string> ();

			column_names.Add ("command");
			column_names.Add ("name");
			column_names.Add ("text");
			column_names.Add ("para1");
			column_names.Add ("para2");
			column_names.Add ("para3");
			column_names.Add ("x");
			column_names.Add ("y");
			column_names.Add ("time");
			column_names.Add ("no");

			int count = 0;

			string output = "";

			foreach( string column_name in column_names){

				if(column_name==input_column_name){

					output=input_row[count];

				}

				count++;

			}


			

			return output;

		

		}

		public string get_column_data_exception(List<string> input_row,string input_action_name,string input_column_name,string input_variable_name){
			string output = "";


			if (get_column_data (input_row, "command") == "Beat") {

				if (input_column_name == "command") {
					output = "beat";
				}

			} else if (get_column_data (input_row, "command") == "Scene") {

				if (input_column_name == "command") {
					output = "scene";
				}

			} else if (get_column_data (input_row, "command") == "Title") {
				
				if (input_column_name == "command") {
					output = "title";
				}

			} else if (get_column_data (input_row, "command") == "Select") {

				if (input_column_name == "text") {



				}
			}

			return output;

		}


		public JSONObject row_to_json(List<string> input_row, string input_action_name){
			JSONObject output = new JSONObject (JSONObject.Type.ARRAY);

			List<column_name_to_variable_name> temp= get_column_names_to_variable_names_list(input_action_name);

			foreach(column_name_to_variable_name c in temp){

				output.AddField(c.variable_name,convert_column_data(input_row,input_action_name,c.column_name,c.variable_name));

			}


			return output;
		
		}

		public List<column_name_to_variable_name> get_column_names_to_variable_names_list(string input_action_name){

			List<column_name_to_variable_name> output = new List<column_name_to_variable_name> ();


			if (input_action_name == "Action_adv_text") {

				output.Add (new column_name_to_variable_name ("name", "person_name"));


				output.Add (new column_name_to_variable_name ("text", "text"));


			} else if (input_action_name == "Effect") {

				output.Add (new column_name_to_variable_name ("command", "effect_type"));

			} else if (input_action_name == "Wait") {

				output.Add (new column_name_to_variable_name ("time", "wait_time"));

			} else if (input_action_name == "Jump") {
				
				output.Add (new column_name_to_variable_name ("command", "jump_type"));

				output.Add (new column_name_to_variable_name ("para1", "destination"));

			}else if(input_action_name=="Select"){

				output.Add (new column_name_to_variable_name ("text", "select_text_0"));
				output.Add (new column_name_to_variable_name ("text", "select_text_1"));
				output.Add (new column_name_to_variable_name ("text", "select_text_2"));

				/*
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));
				output.Add (new column_name_to_variable_name ("text", "jump_tag_0"));*/

			}

			output.Add (new column_name_to_variable_name("tag","tag"));
			

			return output;

			
			
		}


	}

	public class column_name_to_variable_name {

		public string column_name;

		public string variable_name;

		public column_name_to_variable_name(string input_column_name, string input_variable_name){
			column_name = input_column_name;
			variable_name = input_variable_name;
		}


	}
	

}
